<?php


namespace App\Command;


use App\Entity\CustomerRequest;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ArchiveExpiredCustomerRequestsCommand extends Command
{
    private $em;

    protected static $defaultName = 'app:archive_customer_requests';

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Delete all expired customer requests.')
            ->setHelp('Delete all expired customer requests.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $requests = $this->em->getRepository(CustomerRequest::class)->findByExpiredToken();
        $count = 0;

        foreach ($requests as $request) {
            ++$count;
            $request->setArchived(true);
            $this->em->persist($request);
            $output->writeln('Request n° '.$request->getId().' going to be archived.');
        }

        $this->em->flush();

        $output->write($count. ' requests archived.');
        return 0;
    }
}