<?php


namespace App\Command;


use App\RequestDispatcher\Dispatcher;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DispatchCustomerRequestsCommand extends Command
{
    protected static $defaultName = 'app:dispatcher:run';

    private Dispatcher $dispatcher;

    public function __construct(Dispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Dispatch customer requests to providers.')
            ->setHelp('Dispatch customer requests to providers.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->dispatcher->dispatch();
    }
}