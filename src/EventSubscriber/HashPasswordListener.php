<?php


namespace App\EventSubscriber;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class HashPasswordListener implements EventSubscriberInterface
{
    private $encoder;
    private $em;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $em)
    {
        $this->encoder = $passwordEncoder;
        $this->em = $em;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['hashUserPassword', EventPriorities::PRE_WRITE],
        ];
    }

    public function hashUserPassword(ViewEvent $event)
    {
        $user = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();
        $methods = [Request::METHOD_POST, Request::METHOD_PUT];

        if (!$user instanceof User || !in_array($method, $methods)) {
            return;
        }

        /** @var User $dbUser */
        $dbUser = $this->em->getRepository(User::class)->findOneBy(['id' => $user->getId()]);

        if(!is_null($dbUser) && $dbUser->getPassword() === $user->getPassword())
        {
            return;
        }

        $hashPassword = $this->encoder->encodePassword($user, $user->getPassword());
        $user->setPassword($hashPassword);

    }
}