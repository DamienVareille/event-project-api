<?php

namespace App\Repository;

use App\Entity\CustomerRequestTracking;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CustomerRequestTracking|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomerRequestTracking|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomerRequestTracking[]    findAll()
 * @method CustomerRequestTracking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerRequestTrackingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CustomerRequestTracking::class);
    }

    // /**
    //  * @return CustomerRequestTracking[] Returns an array of CustomerRequestTracking objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CustomerRequestTracking
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
