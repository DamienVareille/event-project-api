<?php

namespace App\Repository;

use App\Entity\ProposalRequests;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ProposalRequests|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProposalRequests|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProposalRequests[]    findAll()
 * @method ProposalRequests[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProposalRequestsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProposalRequests::class);
    }

    // /**
    //  * @return ProposalRequests[] Returns an array of ProposalRequests objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProposalRequests
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
