<?php

namespace App\Repository;

use App\Entity\RequestState;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RequestState|null find($id, $lockMode = null, $lockVersion = null)
 * @method RequestState|null findOneBy(array $criteria, array $orderBy = null)
 * @method RequestState[]    findAll()
 * @method RequestState[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RequestStateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RequestState::class);
    }

    // /**
    //  * @return CustomerRequestState[] Returns an array of CustomerRequestState objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CustomerRequestState
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
