<?php

namespace App\Repository;

use App\Entity\CustomerRequest;
use App\Entity\RequestState;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use http\Env\Request;

/**
 * @method CustomerRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomerRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomerRequest[]    findAll()
 * @method CustomerRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CustomerRequest::class);
    }

    // /**
    //  * @return post[] Returns an array of post objects
    //  */

    public function findByExpiredToken()
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.validationTokenExpireAt < :now')
            ->andWhere('c.validated = 0')
            ->andWhere('c.archived = 0')
            ->setParameter('now', new \DateTime('NOW'))
            ->getQuery()
            ->getResult()
        ;
    }

    public function findAllDispatchableRequests()
    {
        return $this->createQueryBuilder('c')
            ->join('c.requestState', 'rs')
            ->where('rs.constantCode = :pending_state')
            ->andWhere('c.validated = 1')
            ->andWhere('c.archived = 0')
            ->setParameter('pending_state', RequestState::PENDING_DISPATCH)
            ->getQuery()
            ->getResult()
        ;
    }

}
