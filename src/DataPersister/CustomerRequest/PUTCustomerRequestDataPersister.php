<?php


namespace App\DataPersister\CustomerRequest;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\CustomerRequest;
use App\Entity\RequestState;
use App\Helpers\StringHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Templating\EngineInterface;

final class PUTCustomerRequestDataPersister implements ContextAwareDataPersisterInterface
{
    private \Swift_Mailer $mailer;
    private EntityManagerInterface $entityManager;
    private EngineInterface $templating;
    private StringHelper $stringHelper;

    public function __construct(\Swift_Mailer $mailer, EntityManagerInterface $entityManager, EngineInterface $templating, StringHelper $stringHelper)
    {
        $this->mailer = $mailer;
        $this->entityManager = $entityManager;
        $this->templating = $templating;
        $this->stringHelper = $stringHelper;
    }

    /**
     * @inheritDoc
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof CustomerRequest && isset($context['item_operation_name']);
    }

    /**
     * @inheritDoc
     */
    public function persist($data, array $context = [])
    {
        if($context['item_operation_name'] !== 'put') {
            return $data;
        }

        $customerRequest = $data;

        if($customerRequest instanceof CustomerRequest && $customerRequest->getValidationTokenExpireAt() > new \DateTime("now") && !$customerRequest->isValidated())
        {
            $customerRequest->setValidated(true);
            $customerRequest->setTrackingNumber($this->stringHelper->generateRandomString(15, false));

            $pending_state = $this->entityManager->getRepository(RequestState::class)->findOneBy(['state' => RequestState::PENDING_DISPATCH]);
            $customerRequest->setRequestState($pending_state);

            $this->entityManager->persist($customerRequest);

            $this->entityManager->flush();

            $message = (new \Swift_Message('Votre demande de devis est en cours de traitement !'))
                ->setFrom('contact@damienvareille.com')
                ->setTo($customerRequest->getCustomer()->getEmail())
                ->setBody($this->templating->render(
                // templates/emails/registration.html.twig
                    'emails/customerRequestActivated.html.twig',
                    ['customer' => $customerRequest->getCustomer(), 'customer_request' => $customerRequest]
                ),
                    'text/html')
            ;

            $this->mailer->send($message);
        }

        return $customerRequest;
    }

    /**
     * @inheritDoc
     */
    public function remove($data, array $context = [])
    {
        // TODO: Implement remove() method.
    }
}