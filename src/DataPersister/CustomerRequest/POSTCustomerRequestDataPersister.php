<?php


namespace App\DataPersister\CustomerRequest;


use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\CustomerRequest;
use App\Entity\RequestState;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\Exception\TransportException;
use Symfony\Component\Templating\EngineInterface;

class POSTCustomerRequestDataPersister implements ContextAwareDataPersisterInterface
{
    private $mailer;
    private $entityManager;
    private $templating;
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    public function __construct(\Swift_Mailer $mailer, EntityManagerInterface $entityManager, EngineInterface $templating, LoggerInterface $logger)
    {
        $this->mailer = $mailer;
        $this->entityManager = $entityManager;
        $this->templating = $templating;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof CustomerRequest;
    }

    /**
     * @inheritDoc
     */
    public function persist($data, array $context = [])
    {
        if(!array_key_exists('collection_operation_name', $context)) {
            return $data;
        }

        /** @var CustomerRequest $customerRequest */
        $customerRequest = $data;

        $notValidatedState = $this->entityManager->getRepository(RequestState::class)->findOneBy(['constantCode' => RequestState::NOT_VALIDATED]);
        $customerRequest->setRequestState($notValidatedState);

        $this->entityManager->persist($customerRequest);
        $this->entityManager->flush();

        try {
            $message = (new \Swift_Message('Confirmez votre demande de prestation'))
                ->setFrom('contact@damienvareille.com')
                ->setTo($customerRequest->getCustomer()->getEmail())
                ->setBody($this->templating->render(
                // templates/emails/registration.html.twig
                    'emails/customerRequest.html.twig',
                    ['customer' => $customerRequest->getCustomer(), 'customer_request' => $customerRequest]
                ),
                    'text/html')
            ;
            $this->mailer->send($message);

            $this->logger->info("[Mailer] Mail création CustomerRequest #{$customerRequest->getId()} à {$customerRequest->getCustomer()->getEmail()}");
        }
        catch (\Exception $e) {
            throw new TransportException("[MailerException] Erreur lors de l'envoi de mail de création CustomerRequest #{$customerRequest->getId()}. Exception : {$e->getMessage()}");
        } finally {
            return $customerRequest;
        }
    }

    /**
     * @inheritDoc
     */
    public function remove($data, array $context = [])
    {
        // TODO: Implement remove() method.
    }

}