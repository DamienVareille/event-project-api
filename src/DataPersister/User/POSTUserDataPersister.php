<?php


namespace App\DataPersister\User;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\CustomerRequest;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Templating\EngineInterface;

class POSTUserDataPersister implements ContextAwareDataPersisterInterface
{
    private $mailer;
    private $entityManager;
    private $templating;

    public function __construct(\Swift_Mailer $mailer, EntityManagerInterface $entityManager, EngineInterface $templating)
    {
        $this->mailer = $mailer;
        $this->entityManager = $entityManager;
        $this->templating = $templating;
    }

    /**
     * @inheritDoc
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof User && isset($context['collection_operation_name']);
    }

    /**
     * @inheritDoc
     */
    public function persist($data, array $context = [])
    {
        if(!array_key_exists('collection_operation_name', $context)) {
            return $data;
        }

        $user = $data;

        if ($user instanceof User)
        {
            if($user->getPassword()) {
                $user->setUsername($user->getEmail());
                $user->setValidationToken($this->generateRandomString());
                $user->setValidationTokenExpireAt(new \DateTime("now + 24 hours"));
                $user->setRoles(["ROLE_PARTIAL_USER"]);
                $this->sendConfirmationEmail($user);
            }
        }

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    /**
     * @inheritDoc
     */
    public function remove($data, array $context = [])
    {
        // TODO: Implement remove() method.
    }

    private function generateRandomString($length = 60) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    private function sendConfirmationEmail($user)
    {
        if (!$user instanceof User) {
            return;
        }

        $email = $user->getEmail();

        $message = (new \Swift_Message('Activez votre compte professionnel !'))
            ->setFrom('contact@damienvareille.com')
            ->setTo($email)
            ->setBody($this->templating->render(
            // templates/emails/registration.html.twig
                'emails/accountValidation.html.twig',
                ['validationToken' => $user->getValidationToken()]
            ),
                'text/html')
        ;

        $this->mailer->send($message);
    }

}