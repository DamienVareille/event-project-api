<?php


namespace App\DataPersister\User;


use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use ApiPlatform\Core\Exception\FilterValidationException;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PUTUserDataPersister implements ContextAwareDataPersisterInterface
{
    private $mailer;
    private $entityManager;
    private $passwordEncoder;

    public function __construct(\Swift_Mailer $mailer, EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->mailer = $mailer;
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @inheritDoc
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof User && isset($context['item_operation_name']);
    }

    /**
     * @inheritDoc
     * @throws FilterValidationException
     */
    public function persist($data, array $context = [])
    {
        if(!array_key_exists('item_operation_name', $context)) {
            return $data;
        }

        /** @var User $user */
        $user = $data;

        if($user->getValidationTokenExpireAt() < new \DateTime("now") && !$user->isActivated()) {
            throw new FilterValidationException(["Le délai d'activation de cet utilisateur a expiré. Merci de recréer un compte."]);
        }
        else if ($user->isActivated())
        {
            $user->setRoles(["ROLE_USER"]);
            $this->entityManager->flush();
        }
        else {
            throw new FilterValidationException(["L'utilisateur n'est pas activé."]);
        }

        return $user;
    }

    /**
     * @inheritDoc
     */
    public function remove($data, array $context = [])
    {
        // TODO: Implement remove() method.
    }
}