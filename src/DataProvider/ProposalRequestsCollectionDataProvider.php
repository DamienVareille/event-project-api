<?php


namespace App\DataProvider;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGenerator;
use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Doctrine\ProposalRequestsExtension;
use App\Entity\ProposalRequests;
use App\Filter\RequestStateFilter;
use Doctrine\Common\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;

class ProposalRequestsCollectionDataProvider implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface
{

    private $extension;
    private $managerRegistry;
    private $log;
    private $collectionExtensions;

    public function __construct(ManagerRegistry $managerRegistry, ProposalRequestsExtension $extension, LoggerInterface $log, RequestStateFilter $collectionExtensions)
    {
        $this->managerRegistry = $managerRegistry;
        $this->extension = $extension;
        $this->log = $log;
        $this->collectionExtensions = $collectionExtensions;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return ProposalRequests::class === $resourceClass;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): iterable
    {
        $manager = $this->managerRegistry->getManagerForClass($resourceClass);
        $repository = $manager->getRepository($resourceClass);
        $queryBuilder = $repository->createQueryBuilder('pr');
        $queryNameGenerator = new QueryNameGenerator();

        $this->extension->applyToCollection($queryBuilder, $queryNameGenerator, $resourceClass, $operationName);
        $this->collectionExtensions->apply($queryBuilder, $queryNameGenerator, $resourceClass, $operationName, $context);

        $results = $queryBuilder->getQuery()->getResult();

        foreach($results as $res) {
            yield $res;
        }

        return [];
    }
}