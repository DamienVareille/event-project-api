<?php


namespace App\RequestDispatcher;


use App\Entity\CustomerRequest;
use App\Entity\ProposalRequests;
use App\Entity\RequestState;
use Doctrine\ORM\EntityManagerInterface;

class Dispatcher
{
    public const PENDING_REQUESTS_DISPATCHED = '0';
    public const NO_PENDING_REQUEST = '1';

    private EntityManagerInterface $em;

    private Logger $logger;

    private ProvidersFinder $finder;

    public function __construct(EntityManagerInterface $em, Logger $logger, ProvidersFinder $finder)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->finder = $finder;
    }

    public function dispatch(): int
    {
        $pendingRequests = $this->em->getRepository(CustomerRequest::class)->findAllDispatchableRequests();

        if (0 === \count($pendingRequests)) {
            $this->logger->info('No pending request. Skipping dispatching...');
            return self::NO_PENDING_REQUEST;
        }

        $pendingState = $this->em->getRepository(RequestState::class)->findOneBy(['constantCode' => RequestState::PENDING]);
        foreach ($pendingRequests as $request) {
            $candidateProviders = $this->finder->findProvidersForRequest($request);

            foreach ($candidateProviders as $p) {
                $proposal = new ProposalRequests();

                $proposal->setCustomerRequest($request);
                $proposal->setProvider($p);
                $proposal->setRequestState($pendingState);
                $proposal->setIsLocalProvider(0);

                $this->em->persist($proposal);
            }
        }

        $this->em->flush();

        return self::PENDING_REQUESTS_DISPATCHED;
    }
}