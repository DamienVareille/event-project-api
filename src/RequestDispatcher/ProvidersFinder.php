<?php


namespace App\RequestDispatcher;


use App\Entity\CustomerRequest;
use App\Entity\ProposalRequests;
use App\Entity\Provider;
use Doctrine\ORM\EntityManagerInterface;

class ProvidersFinder
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function findProvidersForRequest(CustomerRequest $request): array
    {
        $providers = [];
        $providers = $this->em->getRepository(Provider::class)->findByProviderTypes($request->getProviderTypes());

        return $providers;
    }

}