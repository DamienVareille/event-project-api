<?php


namespace App\DataFixtures;


use App\Entity\RequestState;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class RequestStateFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $stateNames = ["En attente", "Acceptée", "Non validée", "Clôturée", "Refusée", "En attente de distribution"];
        $stateCodes = ["PENDING", "ACCEPTED", "NOT_VALIDATED", "FENCED", "REFUTED", "PENDING_DISPATCH"];
        $stateBadges = ["warning", "success", "info", "dark", "danger", "warning"];

        for($i = 0; $i < 6; ++$i) {
            $state = new RequestState();
            $state->setConstantCode($stateCodes[$i]);
            $state->setName($stateNames[$i]);
            $state->setFrontBadgeType($stateBadges[$i]);

            $manager->persist($state);
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['base'];
    }
}