<?php

namespace App\DataFixtures;

use App\Entity\City;
use App\Entity\Provider;
use App\Entity\ProviderType;
use App\Entity\User;
use App\Helpers\StringHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use Faker\Provider\DateTime;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ProviderFixtures extends Fixture implements FixtureGroupInterface
{
    private UserPasswordEncoderInterface $passwordEncoder;

    private StringHelper $stringHelper;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, StringHelper $stringHelper)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->stringHelper = $stringHelper;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        for($i=0; $i < 30; $i++)
        {
            $pt = new ProviderType();
            $pt->setType($faker->jobTitle);
            $pt->setAvailable(rand(0,1));
            $manager->persist($pt);
        }

        $manager->flush();
        $cities = $manager->getRepository(City::class)->findAll();
        $pts = $manager->getRepository(ProviderType::class)->findAll();

        for($i=0; $i < 2; $i++)
        {
            shuffle($cities);
            shuffle($pts);
            $user = new User();
            if (0 === $i) {
                $user->setRoles(['ROLE_USER']);
            }
            if (1 === $i) {
                $user->setRoles(['ROLE_ADMIN']);
            }
            $user->setActivated(1);
            $user->setUsername($this->stringHelper->generateRandomString(5));
            $user->setEmail($faker->email);
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                'test'
            ));
            $user->setValidationToken($this->stringHelper->generateRandomString(15, true));
            $provider = new Provider();
            $provider->setUser($user);
            $provider->setCompany($faker->company);
            $provider->setDescription($faker->realText(150));
            $provider->setPhoneNumber($faker->phoneNumber);
            $provider->setProfilePicture('default.png');
            $provider->setName($faker->lastName);
            $provider->setSurname($faker->firstName);
            $provider->setActionRange($faker->randomDigit);
            $provider->setCity($cities[0]);
            $provider->addProviderType($pts[0]);
            $manager->persist($provider);
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['base'];
    }
}
