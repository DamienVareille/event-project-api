<?php


namespace App\DataFixtures;


use App\Entity\City;
use App\Entity\Customer;
use App\Entity\Provider;
use App\Entity\ProviderType;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use Faker\Provider\DateTime;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CustomerFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        $cities = $manager->getRepository(City::class)->findAll();

        for($i=0; $i < 1; $i++)
        {
            shuffle($cities);
            $customer = new Customer();
            $customer->setName($faker->lastName);
            $customer->setSurname($faker->firstName);
            $customer->setSocialReason($faker->company);
            $customer->setEmail($faker->email);
            $customer->setCity($cities[0]);
            $customer->setAdditionalAddress($faker->secondaryAddress);
            $customer->setStreetName($faker->streetName);
            $customer->setStreetNumber($faker->numberBetween(1,99));
            $customer->setPhoneNumber("0555028881");
            $manager->persist($customer);
        }

        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return array(
            CityFixtures::class,
        );
    }

    public static function getGroups(): array
    {
        return ['base'];
    }
}