<?php


namespace App\DataFixtures;


use App\Entity\City;
use App\Entity\Customer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class CityFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        for($i=0; $i < 50; $i++)
        {
            $city = new City();
            $city->setCityNom($faker->city);
            $city->setCityCodePostal($faker->postcode);
            $city->setCityInseeCode($city->getCityCodePostal());
            $city->setCityNomReel($city->getCityNom());
            $manager->persist($city);
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['base'];
    }
}