<?php


namespace App\DataFixtures;


use App\Entity\CustomerRequest;
use App\Entity\ProposalRequests;
use App\Entity\Provider;
use App\Entity\RequestState;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class ProposalRequestFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{

    public function load(ObjectManager $manager)
    {

        $providers = $manager->getRepository(Provider::class)->findAll();
        $requests = $manager->getRepository(CustomerRequest::class)->findAll();

        $pendingState = $manager->getRepository(RequestState::class)->findOneBy(['constantCode' => RequestState::PENDING]);

        $req = new ProposalRequests();

        $req->setCustomerRequest($requests[0]);
        $req->setProvider($providers[0]);
        $req->setIsLocalProvider(rand(0,1));
        $req->setPropositionSentByProvider(rand(0,1));
        $req->setRequestState($pendingState);

        $manager->persist($req);

        $req2 = new ProposalRequests();
        $req2->setCustomerRequest($requests[0]);
        $req2->setProvider($providers[0]);
        $req2->setIsLocalProvider(rand(0,1));
        $req2->setPropositionSentByProvider(rand(0,1));

        $manager->persist($req2);

        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return array(
            ProviderFixtures::class,
        );
    }

    public static function getGroups(): array
    {
        return ['base'];
    }
}