<?php

namespace App\DataFixtures;

use App\Entity\CustomerRequest;
use App\Entity\RequestState;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AddPendingCustomerRequestFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var CustomerRequest $cr */
        $cr = $manager->getRepository(CustomerRequest::class)->findAll()[0];
        $pendingState = $manager->getRepository(RequestState::class)->findOneBy(['constantCode' => RequestState::PENDING_DISPATCH]);
        $cr->setRequestState($pendingState);
        $cr->setArchived(false);
        $cr->setValidated(true);
        $manager->persist($cr);
        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['add_pending_customer_request'];
    }
}