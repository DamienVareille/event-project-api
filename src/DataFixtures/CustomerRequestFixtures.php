<?php


namespace App\DataFixtures;


use App\Entity\City;
use App\Entity\Customer;
use App\Entity\CustomerRequest;
use App\Entity\RequestState;
use App\Entity\Provider;
use App\Helpers\StringHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class CustomerRequestFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{

    private StringHelper $stringHelper;

    public function __construct(StringHelper $stringHelper)
    {
        $this->stringHelper = $stringHelper;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        $cities = $manager->getRepository(City::class)->findAll();
        $customers = $manager->getRepository(Customer::class)->findAll();
        $states = $manager->getRepository(RequestState::class)->findAll();

        $validated = rand(0,1);

        for($i=0; $i < 1; $i++)
        {
            shuffle($cities);
            shuffle($customers);
            $req = new CustomerRequest();
            $req->setDate($faker->dateTime);
            $req->setPeopleNumber($faker->randomNumber(3));
            $req->setBudget("450€");
            $req->setDescription($faker->text(150));
            $req->setDuration("2h");
            $req->setPlace($cities[0]);
            $req->setCustomer($customers[0]);
            $req->setTime($faker->dateTime);
            $req->setValidationToken($this->stringHelper->generateRandomString());

            if($validated)
            {
                $req->setValidated(1);
                $req->setTrackingNumber($this->stringHelper->generateRandomString(6, false));
                $req->setValidationTokenExpireAt(new \DateTime('NOW - 24 hours'));
            } else {
                $req->setValidationTokenExpireAt(new \DateTime('NOW + 24 hours'));
            }

            $req->setRequestState($states[1]);

            $manager->persist($req);
        }

        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return array(
            RequestStateFixtures::class,
        );
    }

    public static function getGroups(): array
    {
        return ['base'];
    }
}