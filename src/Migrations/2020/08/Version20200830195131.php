<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200830195131 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("INSERT INTO customer_request_state(created_at, updated_at, name, constant_code, front_badge_type) VALUES(NOW(), NOW(), 'En attente de validation', 'NOT_VALIDATED', 'warning')");
    }

    public function down(Schema $schema) : void
    {
    }
}
