<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200818152521 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE proposal_request_state (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(30) NOT NULL, constant_code VARCHAR(30) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE proposal_requests ADD proposal_request_state_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE proposal_requests ADD CONSTRAINT FK_9ADEEA79C6507C7A FOREIGN KEY (proposal_request_state_id) REFERENCES proposal_request_state (id)');
        $this->addSql('CREATE INDEX IDX_9ADEEA79C6507C7A ON proposal_requests (proposal_request_state_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal_requests DROP FOREIGN KEY FK_9ADEEA79C6507C7A');
        $this->addSql('DROP TABLE proposal_request_state');
        $this->addSql('DROP INDEX IDX_9ADEEA79C6507C7A ON proposal_requests');
        $this->addSql('ALTER TABLE proposal_requests DROP proposal_request_state_id');
    }
}
