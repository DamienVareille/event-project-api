<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\Migrations\Exception\MigrationException;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200818153235 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("INSERT INTO customer_request_state(name, constant_code, created_at, updated_at) VALUES('Non validée', 'NOT_VALIDATED', NOW(), NOW())");
        $this->addSql("INSERT INTO customer_request_state(name, constant_code, created_at, updated_at) VALUES('En attente', 'PENDING', NOW(), NOW())");
        $this->addSql("INSERT INTO customer_request_state(name, constant_code, created_at, updated_at) VALUES('Acceptée', 'ACCEPTED', NOW(), NOW())");
        $this->addSql("INSERT INTO customer_request_state(name, constant_code, created_at, updated_at) VALUES('Clôturé', 'FENCED', NOW(), NOW())");
    }

    public function down(Schema $schema): void
    {
        // TODO: Implement down() method.
    }
}
