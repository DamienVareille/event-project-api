<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200821163041 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("UPDATE customer_request_state SET front_badge_type = 'warning' WHERE constant_code='PENDING'");
        $this->addSql("INSERT INTO customer_request_state(created_at, updated_at, name, constant_code, front_badge_type) VALUES (NOW(), NOW(), 'Refusée', 'REFUTED', 'danger')");
    }

    public function down(Schema $schema) : void
    {

    }
}
