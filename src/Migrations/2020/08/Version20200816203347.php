<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200816203347 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE customer_request (id INT AUTO_INCREMENT NOT NULL, place_id INT UNSIGNED NOT NULL, customer_id INT DEFAULT NULL, state_id INT DEFAULT NULL, date DATE NOT NULL, time TIME DEFAULT NULL, people_number INT DEFAULT NULL, budget VARCHAR(6) DEFAULT NULL, description LONGTEXT NOT NULL, duration VARCHAR(30) DEFAULT NULL, validation_token VARCHAR(255) DEFAULT NULL, validation_token_expire_at DATETIME NOT NULL, validated TINYINT(1) DEFAULT \'0\' NOT NULL, tracking_number VARCHAR(20) DEFAULT NULL, archived TINYINT(1) DEFAULT \'0\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_274A2B21DA6A219 (place_id), INDEX IDX_274A2B219395C3F3 (customer_id), INDEX IDX_274A2B215D83CC1 (state_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE requests_provider_types (customer_request_id INT NOT NULL, provider_type_id INT NOT NULL, INDEX IDX_5F5A8226BFB7BC27 (customer_request_id), INDEX IDX_5F5A822635142E34 (provider_type_id), PRIMARY KEY(customer_request_id, provider_type_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE customer_request_state (id INT AUTO_INCREMENT NOT NULL, state VARCHAR(40) NOT NULL, state_name VARCHAR(40) NOT NULL, code VARCHAR(40) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE city (id INT UNSIGNED AUTO_INCREMENT NOT NULL, city_nom VARCHAR(45) DEFAULT NULL, city_nom_reel VARCHAR(45) DEFAULT NULL, city_code_postal VARCHAR(255) DEFAULT NULL, city_insee_code VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE proposal_requests (id INT AUTO_INCREMENT NOT NULL, provider_id INT DEFAULT NULL, customer_request_id INT DEFAULT NULL, proposition_sent_by_provider TINYINT(1) DEFAULT NULL, is_local_provider TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_9ADEEA79A53A8AA (provider_id), INDEX IDX_9ADEEA79BFB7BC27 (customer_request_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, user_type_id INT DEFAULT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, username VARCHAR(100) NOT NULL, activated TINYINT(1) NOT NULL, validation_token VARCHAR(255) NOT NULL, validation_token_expire_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), INDEX IDX_8D93D6499D419299 (user_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE customer (id INT AUTO_INCREMENT NOT NULL, city_id INT UNSIGNED NOT NULL, name VARCHAR(50) NOT NULL, surname VARCHAR(50) NOT NULL, social_reason VARCHAR(50) DEFAULT NULL, email VARCHAR(180) NOT NULL, phone_number VARCHAR(12) NOT NULL, street_number VARCHAR(6) NOT NULL, street_name VARCHAR(255) NOT NULL, additional_address VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_81398E098BAC62AF (city_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE provider_type (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(150) NOT NULL, available TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_type (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(30) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE provider (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, city_id INT UNSIGNED DEFAULT NULL, name VARCHAR(255) NOT NULL, surname VARCHAR(255) NOT NULL, company VARCHAR(255) DEFAULT NULL, phone_number VARCHAR(50) DEFAULT NULL, action_range INT DEFAULT NULL, profile_picture VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_92C4739CA76ED395 (user_id), INDEX IDX_92C4739C8BAC62AF (city_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE providers_list_types (provider_id INT NOT NULL, provider_type_id INT NOT NULL, INDEX IDX_3B4CF6C0A53A8AA (provider_id), INDEX IDX_3B4CF6C035142E34 (provider_type_id), PRIMARY KEY(provider_id, provider_type_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE refresh_tokens (id INT AUTO_INCREMENT NOT NULL, refresh_token VARCHAR(128) NOT NULL, username VARCHAR(255) NOT NULL, valid DATETIME NOT NULL, UNIQUE INDEX UNIQ_9BACE7E1C74F2195 (refresh_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE customer_request ADD CONSTRAINT FK_274A2B21DA6A219 FOREIGN KEY (place_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE customer_request ADD CONSTRAINT FK_274A2B219395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('ALTER TABLE customer_request ADD CONSTRAINT FK_274A2B215D83CC1 FOREIGN KEY (state_id) REFERENCES customer_request_state (id)');
        $this->addSql('ALTER TABLE requests_provider_types ADD CONSTRAINT FK_5F5A8226BFB7BC27 FOREIGN KEY (customer_request_id) REFERENCES customer_request (id)');
        $this->addSql('ALTER TABLE requests_provider_types ADD CONSTRAINT FK_5F5A822635142E34 FOREIGN KEY (provider_type_id) REFERENCES provider_type (id)');
        $this->addSql('ALTER TABLE proposal_requests ADD CONSTRAINT FK_9ADEEA79A53A8AA FOREIGN KEY (provider_id) REFERENCES provider (id)');
        $this->addSql('ALTER TABLE proposal_requests ADD CONSTRAINT FK_9ADEEA79BFB7BC27 FOREIGN KEY (customer_request_id) REFERENCES customer_request (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6499D419299 FOREIGN KEY (user_type_id) REFERENCES user_type (id)');
        $this->addSql('ALTER TABLE customer ADD CONSTRAINT FK_81398E098BAC62AF FOREIGN KEY (city_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE provider ADD CONSTRAINT FK_92C4739CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE provider ADD CONSTRAINT FK_92C4739C8BAC62AF FOREIGN KEY (city_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE providers_list_types ADD CONSTRAINT FK_3B4CF6C0A53A8AA FOREIGN KEY (provider_id) REFERENCES provider (id)');
        $this->addSql('ALTER TABLE providers_list_types ADD CONSTRAINT FK_3B4CF6C035142E34 FOREIGN KEY (provider_type_id) REFERENCES provider_type (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE requests_provider_types DROP FOREIGN KEY FK_5F5A8226BFB7BC27');
        $this->addSql('ALTER TABLE proposal_requests DROP FOREIGN KEY FK_9ADEEA79BFB7BC27');
        $this->addSql('ALTER TABLE customer_request DROP FOREIGN KEY FK_274A2B215D83CC1');
        $this->addSql('ALTER TABLE customer_request DROP FOREIGN KEY FK_274A2B21DA6A219');
        $this->addSql('ALTER TABLE customer DROP FOREIGN KEY FK_81398E098BAC62AF');
        $this->addSql('ALTER TABLE provider DROP FOREIGN KEY FK_92C4739C8BAC62AF');
        $this->addSql('ALTER TABLE provider DROP FOREIGN KEY FK_92C4739CA76ED395');
        $this->addSql('ALTER TABLE customer_request DROP FOREIGN KEY FK_274A2B219395C3F3');
        $this->addSql('ALTER TABLE requests_provider_types DROP FOREIGN KEY FK_5F5A822635142E34');
        $this->addSql('ALTER TABLE providers_list_types DROP FOREIGN KEY FK_3B4CF6C035142E34');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6499D419299');
        $this->addSql('ALTER TABLE proposal_requests DROP FOREIGN KEY FK_9ADEEA79A53A8AA');
        $this->addSql('ALTER TABLE providers_list_types DROP FOREIGN KEY FK_3B4CF6C0A53A8AA');
        $this->addSql('DROP TABLE customer_request');
        $this->addSql('DROP TABLE requests_provider_types');
        $this->addSql('DROP TABLE customer_request_state');
        $this->addSql('DROP TABLE city');
        $this->addSql('DROP TABLE proposal_requests');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE customer');
        $this->addSql('DROP TABLE provider_type');
        $this->addSql('DROP TABLE user_type');
        $this->addSql('DROP TABLE provider');
        $this->addSql('DROP TABLE providers_list_types');
        $this->addSql('DROP TABLE refresh_tokens');
    }
}
