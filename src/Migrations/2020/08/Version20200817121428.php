<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200817121428 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE providers_types (provider_id INT NOT NULL, provider_type_id INT NOT NULL, INDEX IDX_8C24D5D4A53A8AA (provider_id), INDEX IDX_8C24D5D435142E34 (provider_type_id), PRIMARY KEY(provider_id, provider_type_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE providers_types ADD CONSTRAINT FK_8C24D5D4A53A8AA FOREIGN KEY (provider_id) REFERENCES provider (id)');
        $this->addSql('ALTER TABLE providers_types ADD CONSTRAINT FK_8C24D5D435142E34 FOREIGN KEY (provider_type_id) REFERENCES provider_type (id)');
        $this->addSql('DROP TABLE providers_list_types');
        $this->addSql('ALTER TABLE customer_request CHANGE validation_token validation_token VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE providers_list_types (provider_id INT NOT NULL, provider_type_id INT NOT NULL, INDEX IDX_3B4CF6C035142E34 (provider_type_id), INDEX IDX_3B4CF6C0A53A8AA (provider_id), PRIMARY KEY(provider_id, provider_type_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE providers_list_types ADD CONSTRAINT FK_3B4CF6C035142E34 FOREIGN KEY (provider_type_id) REFERENCES provider_type (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE providers_list_types ADD CONSTRAINT FK_3B4CF6C0A53A8AA FOREIGN KEY (provider_id) REFERENCES provider (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('DROP TABLE providers_types');
        $this->addSql('ALTER TABLE customer_request CHANGE validation_token validation_token VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_unicode_ci`');
    }
}
