<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200820202151 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposal_requests DROP FOREIGN KEY FK_9ADEEA79C6507C7A');
        $this->addSql('DROP TABLE proposal_request_state');
        $this->addSql('DROP INDEX IDX_9ADEEA79C6507C7A ON proposal_requests');
        $this->addSql('ALTER TABLE proposal_requests CHANGE proposal_request_state_id request_state_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE proposal_requests ADD CONSTRAINT FK_9ADEEA79F0FDDCE4 FOREIGN KEY (request_state_id) REFERENCES customer_request_state (id)');
        $this->addSql('CREATE INDEX IDX_9ADEEA79F0FDDCE4 ON proposal_requests (request_state_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE proposal_request_state (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(30) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`, constant_code VARCHAR(30) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE proposal_requests DROP FOREIGN KEY FK_9ADEEA79F0FDDCE4');
        $this->addSql('DROP INDEX IDX_9ADEEA79F0FDDCE4 ON proposal_requests');
        $this->addSql('ALTER TABLE proposal_requests CHANGE request_state_id proposal_request_state_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE proposal_requests ADD CONSTRAINT FK_9ADEEA79C6507C7A FOREIGN KEY (proposal_request_state_id) REFERENCES proposal_request_state (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_9ADEEA79C6507C7A ON proposal_requests (proposal_request_state_id)');
    }
}
