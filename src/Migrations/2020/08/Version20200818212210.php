<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200818212210 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("UPDATE customer_request_state SET front_badge_type = 'danger' WHERE constant_code='PENDING'");
        $this->addSql("UPDATE customer_request_state SET front_badge_type = 'success' WHERE constant_code='ACCEPTED'");
        $this->addSql("UPDATE customer_request_state SET front_badge_type = 'dark' WHERE constant_code='FENCED'");
        $this->addSql("UPDATE customer_request_state SET front_badge_type = 'info' WHERE constant_code='NOT_VALIDATED'");
    }

    public function down(Schema $schema) : void
    {

    }
}
