<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200925132624 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("DELETE FROM customer_request_state WHERE constant_code= 'NOT_VALIDATED' AND front_badge_type='warning'");
    }

    public function down(Schema $schema) : void
    {

    }
}
