<?php


namespace App\EventListener;


use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTDecodedEvent;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class JWTDecodedListener extends JWTDecodedEvent
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    private $em;

    /**
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack, EntityManagerInterface $em)
    {
        $this->requestStack = $requestStack;
        $this->em = $em;
        parent::__construct([$requestStack]);
    }

    public function onJWTDecoded(JWTDecodedEvent $event)
    {
        $user_email = $event->getPayload()['email'];

        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $user_email]);

        if ($user !== null) {
            if (!$user->isActivated()) {
                $event->markAsInvalid();
            }
        }
        else {
            $event->markAsInvalid();
        }

    }
}