<?php

namespace App\EventListener;

use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationFailureResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Contracts\Translation\TranslatorInterface;

class AuthenticationListener {

    private $translator;
    private $log;

    public function __construct(TranslatorInterface $translator, LoggerInterface $log)
    {
        $this->translator = $translator;
        $this->log = $log;
    }

    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $user = $event->getUser();
        if (!$user instanceof User)
        {
            return;
        }

        if (!$user->isActivated())
        {
            throw new AccessDeniedException();
        }
    }

    /**
     * @param AuthenticationFailureEvent $event
     */
    public function onAuthenticationFailureResponse(AuthenticationFailureEvent $event)
    {
        $eventMessage = $event->getException()->getMessage();

        $message = isset($eventMessage) ? $eventMessage : $this->translator->trans('bad_credentials');

        if ($event->getException() instanceof BadCredentialsException)
        {
            $message = $this->translator->trans('bad_credentials');
        }

        $data = [
            'message' => $message,
        ];

        $response = new JWTAuthenticationFailureResponse($data);

        $event->setResponse($response);
    }
}

