<?php


namespace App\Doctrine;


use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\ProposalRequests;
use Doctrine\ORM\QueryBuilder;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Security;

class ProposalRequestsExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{
    private $security;
    private $log;

    public function __construct(Security $security, LoggerInterface $log)
    {
        $this->security = $security;
        $this->log = $log;
    }

    public function applyToCollection(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    public function applyToItem(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, array $identifiers, string $operationName = null, array $context = [])
    {
        //$this->addWhere($queryBuilder, $resourceClass);
    }

    private function addWhere(QueryBuilder $queryBuilder, string $resourceClass): void
    {
        if (ProposalRequests::class !== $resourceClass || !$this->security->isGranted('ROLE_USER') || null === $user = $this->security->getUser()) {
            return;
        }

        // Un admin accède à toutes les ressources
        if(!$this->security->isGranted("ROLE_ADMIN"))
        {
            $queryBuilder->innerJoin('pr.provider', 'p');
            $queryBuilder->innerJoin('p.user', 'u', 'WITH', 'u.username = :username OR u.email = :username');
            $queryBuilder->setParameter('username', $user->getUsername());
        }
    }
}