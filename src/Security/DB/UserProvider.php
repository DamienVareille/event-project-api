<?php

namespace App\Security\DB;

use App\Entity\User;
use App\Exception\AccountDisabledException;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserProvider implements UserProviderInterface
{
    /**
     * @var ManagerRegistry
     */
    protected $doctrine;
    private $translator;

    public function __construct( ManagerRegistry $doctrine, TranslatorInterface $translator)
    {
        $this->doctrine = $doctrine;
        $this->translator = $translator;
        return $this;
    }

    /**
     * @param ManagerRegistry $doctrine
     *
     * @return $this
     */
    final public function setDoctrine(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;

        return $this;
    }

    /**
     * Loads the user for the given username.
     *
     * This method must throw UsernameNotFoundException if the user is not
     * found.
     *
     * @param string $username The username
     *
     * @return object
     *
     * @throws UsernameNotFoundException if the user is not found
     */
    public function loadUserByUsername($username)
    {
        if (is_numeric($username)) {
            $user = $this->doctrine->getManager()->getRepository(User::class)->find($username);
        } elseif (filter_var($username, FILTER_VALIDATE_EMAIL)) {
            $user = $this->doctrine->getManager()->getRepository(User::class)->findOneBy(['email' => $username]);
        } else {
            $user = $this->doctrine->getManager()->getRepository(User::class)->findOneBy(['username' => $username]);
        }

        if (!$user) {
            throw new UsernameNotFoundException($this->translator->trans('bad_credentials'), Response::HTTP_UNAUTHORIZED);
        }

        if (!$user->isActivated()) {
            throw new AccountDisabledException($this->translator->trans('account_disabled'), Response::HTTP_UNAUTHORIZED);
        }

        return $user;
    }

    /**
     * Refreshes the user.
     *
     * It is up to the implementation to decide if the user data should be
     * totally reloaded (e.g. from the database), or if the UserInterface
     * object can just be merged into some internal array of users / identity
     * map.
     *
     * @param UserInterface $user
     *
     * @return object
     *
     * @throws UnsupportedUserException if the user is not supported
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * Whether this provider supports the given user class.
     *
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return User::class === $class;
    }
}