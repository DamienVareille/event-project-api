<?php


namespace App\Security\Voters;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class UserVoter extends Voter
{
    const PUT_ITEM = 'put_user_item';

    private $security;
    private $em;
    private $log;

    public function __construct(Security $security, EntityManagerInterface $em, LoggerInterface $log)
    {
        $this->security = $security;
        $this->em = $em;
        $this->log = $log;
    }

    /**
     * @inheritDoc
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [self::PUT_ITEM])) {
            return false;
        }

        if (!$subject instanceof User && !$subject instanceof \Generator) {
            return false;
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /** @var User $req */
        $req = $subject;

        switch ($attribute) {
            case self::PUT_ITEM:
                return $this->canPutItem($req);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canPutItem(User $req)
    {
        /** @var User $user */
        $user = $req;

        // Soit le user peut encore être activé soit il l'est déjà
        if (!$user->isActivated() && ($user->getValidationTokenExpireAt() > new \DateTime("now") || !$this->security->isGranted('ROLE_USER')))
        {
            return true;
        }

        // Le user est connecté est il veut modifier son propre compte (et pas celui d'un autre)
        // NOTE : le username security est configuré pour correspondre au mail de la BDD
        if($this->security->getUser() !== null)
        {
            if($this->security->isGranted('ROLE_USER') && $this->security->getUser()->getUsername() === $user->getEmail())
            {
                return true;
            }
        }

        return false;
    }
}