<?php


namespace App\Security\Voters;


use App\Entity\ProposalRequests;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Lexik\Bundle\JWTAuthenticationBundle\Security\User\JWTUser;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class ProposalRequestsVoter extends Voter
{
    const VIEW_COLLECTION = 'view_proposal_collection';
    const VIEW_ITEM = 'view_proposal_item';

    private $security;
    private $log;
    private $em;

    public function __construct(Security $security, LoggerInterface $log, EntityManagerInterface $em)
    {
        $this->security = $security;
        $this->log = $log;
        $this->em = $em;
    }

    /**
     * @inheritDoc
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [self::VIEW_COLLECTION, self::VIEW_ITEM])) {
            return false;
        }

        if (!$subject instanceof ProposalRequests && !$subject instanceof \Generator) {
            return false;
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof JWTUser) {
            // the user must be logged in; if not, deny access
            return false;
        }

        /** @var ProposalRequests $req */
        $req = $subject;

        switch ($attribute) {
            case self::VIEW_COLLECTION:
                return $this->canViewCollection($user);
            case self::VIEW_ITEM:
                return $this->canViewItem($user, $req);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canViewCollection(JWTUser $user)
    {
        if (!$this->security->isGranted("ROLE_USER")) {
            return false;
        }
        return true;
    }

    private function canViewItem(JWTUser $user, ProposalRequests $req)
    {
        if (!$this->security->isGranted("ROLE_USER")) {
            return false;
        }

        $qb = $this->em->createQueryBuilder();
        $qb->select('pr');
        $qb->from('App:ProposalRequests', 'pr');
        $qb->innerJoin('pr.provider', 'p');
        $qb->innerJoin('p.user', 'u', 'WITH', 'u.username = :username OR u.email = :username');
        $qb->setParameter('username', $user->getUsername());

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
        }

        if(!isset($result)) {
            return false;
        }

        if ($result->getId() !== $req->getId()) {
            return false;
        }

        return true;
    }
}