<?php

namespace App\Controller\Admin;

use App\Entity\CustomerRequest;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TimeField;

class CustomerRequestCrudController extends AbstractCrudController
{

    public static function getEntityFqcn(): string
    {
        return CustomerRequest::class;
    }

    public function createEntity(string $entityFqcn)
    {
        return new CustomerRequest();
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('place'),
            AssociationField::new('customer'),
            AssociationField::new('providerTypes'),
            DateField::new('date'),
            TimeField::new('time'),
            IntegerField::new('peopleNumber'),
            TextField::new('budget'),
            TextField::new('description'),
            TextField::new('duration'),
            TextField::new('duration'),
        ];
    }
}
