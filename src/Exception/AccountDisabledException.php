<?php

namespace App\Exception;

use RuntimeException;

/**
 * Triggered when Country entity related errors occur.
 */
final class AccountDisabledException extends RuntimeException
{
}