<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"read:requestState"}},
 *     collectionOperations={
 *          "get"  = {"security"="is_granted('ROLE_USER')"},
 *          "post" = {"security"="is_granted('ROLE_ADMIN')"}
 *     },
 *     itemOperations={
 *          "get"    = {"security"="is_granted('ROLE_USER')"},
 *          "delete" = {"security"="is_granted('ROLE_ADMIN')"},
 *          "put"    = {"security"="is_granted('ROLE_ADMIN')"},
 *     },
 * )
 * @ORM\Table(name="customer_request_state")
 * @ORM\Entity(repositoryClass="App\Repository\RequestStateRepository")
 * @ApiFilter(SearchFilter::class, properties={"constantCode": "exact"})
 */
class RequestState extends BaseEntity
{
    public const PENDING = 'PENDING';
    public const PENDING_DISPATCH = 'PENDING_DISPATCH';
    public const NOT_VALIDATED = 'NOT_VALIDATED';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=40)
     * @Groups("read:proposalRequest")
     * @Groups("read:requestState")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=40)
     * @Groups("read:proposalRequest")
     * @Groups("read:requestState")
     */
    private $constantCode;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Groups("read:proposalRequest")
     * @Groups("read:requestState")
     */
    private $frontBadgeType;

    public function __construct()
    {
        parent::__construct();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getConstantCode()
    {
        return $this->constantCode;
    }

    /**
     * @param mixed $constantCode
     */
    public function setConstantCode($constantCode): void
    {
        $this->constantCode = $constantCode;
    }

    public function getFrontBadgeType(): ?string
    {
        return $this->frontBadgeType;
    }

    public function setFrontBadgeType(?string $frontBadgeType): self
    {
        $this->frontBadgeType = $frontBadgeType;

        return $this;
    }
}
