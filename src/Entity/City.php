<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiProperty;

/**
 * City
 * @ApiResource(
 *     collectionOperations={
 *          "get",
 *          "post" = {"security"="is_granted('ROLE_ADMIN')"}
 *     },
 *     itemOperations={
 *          "get",
 *          "delete" = {"security"="is_granted('ROLE_ADMIN')"},
 *          "put"    = {"security"="is_granted('ROLE_ADMIN')"},
 *     },)
 * @ApiFilter(SearchFilter::class, properties={"cityNomReel": "start", "cityCodePostal": "start"})
 * @ApiFilter(OrderFilter::class, properties={"cityNomReel"}, arguments={"orderParameterName"="order"})
 * @ORM\Table(name="city")
 * @ORM\Entity(repositoryClass="App\Repository\CityRepository")
 */
class City extends BaseEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ApiProperty(identifier=true)
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="city_nom", type="string", length=45, nullable=true)
     * @Groups("read:proposalRequest")
     * @Groups("read:customerRequest")
     */
    private $cityNom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="city_nom_reel", type="string", length=45, nullable=true)
     * @Groups("read:provider")
     */
    private $cityNomReel;

    /**
     * @var string|null
     *
     * @ORM\Column(name="city_code_postal", type="string", length=255, nullable=true)
     * @Groups("read:provider")
     * @Groups("read:proposalRequest")
     * @Groups("read:customerRequest")
     */
    private $cityCodePostal;

     /**
     * @var string|null
     *
     * @ORM\Column(name="city_insee_code", type="string", length=255, nullable=true)
     */
    private $cityInseeCode;

    /**
     * @ORM\OneToMany(targetEntity="CustomerRequest", mappedBy="place")
     * @Groups("read:customerRequest")
     */
    private $customerRequests;

    public function __construct()
    {
        parent::__construct();
        $this->customerRequests = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getCityNom(): ?string
    {
        return $this->cityNom;
    }

    /**
     * @param string|null $cityNom
     */
    public function setCityNom(?string $cityNom): void
    {
        $this->cityNom = $cityNom;
    }

    /**
     * @return string|null
     */
    public function getCityNomReel(): ?string
    {
        return $this->cityNomReel;
    }

    /**
     * @param string|null $cityNomReel
     */
    public function setCityNomReel(?string $cityNomReel): void
    {
        $this->cityNomReel = $cityNomReel;
    }

    /**
     * @return string|null
     */
    public function getCityCodePostal(): ?string
    {
        return $this->cityCodePostal;
    }

    /**
     * @param string|null $cityCodePostal
     */
    public function setCityCodePostal(?string $cityCodePostal): void
    {
        $this->cityCodePostal = $cityCodePostal;
    }

    /**
     * @return string|null
     */
    public function getCityInseeCode(): ?string
    {
        return $this->cityInseeCode;
    }

    /**
     * @param string|null $cityInseeCode
     */
    public function setCityInseeCode(?string $cityInseeCode): void
    {
        $this->cityInseeCode = $cityInseeCode;
    }

    public function __toString()
    {
        return $this->cityNomReel;
    }

}
