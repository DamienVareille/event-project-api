<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Helpers\StringHelper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiSubresource;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"read:customerRequest"}},
 *     collectionOperations={
 *          "get"  = {"security"="is_granted('ROLE_USER')"},
 *          "post"
 *     },
 *     itemOperations={
 *          "get"    = {"security"="is_granted('ROLE_USER')"},
 *          "delete" = {"security"="is_granted('ROLE_ADMIN')"},
 *          "put"
 *     },
 * )
 * @ApiFilter(SearchFilter::class, properties={"validationToken": "exact", "tracking_number": "exact"})
 * @ORM\Entity(repositoryClass="App\Repository\CustomerRequestRepository")
 */
class CustomerRequest extends BaseEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @ApiProperty(identifier=false)
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="date")
     * @Assert\DateTime
     * @Assert\NotNull
     * @Assert\NotBlank
     * @Groups("read:proposalRequest")
     */
    private ?\DateTimeInterface $date;

    /**
     * @ORM\Column(type="time", nullable=true)
     * @Assert\DateTime
     * @Groups("read:proposalRequest")
     */
    private $time;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\City", inversedBy="customerRequests")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull
     * @Assert\NotBlank
     * @Groups("read:proposalRequest")
     * @Groups("read:customerRequest")
     */
    private ?City $place;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups("read:proposalRequest")
     */
    private ?int $peopleNumber;

    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     * @Assert\Length(max=6)
     * @Groups("read:proposalRequest")
     */
    private ?string $budget;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotNull
     * @Assert\NotBlank
     * @Groups("read:proposalRequest")
     */
    private ?string $description;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     * @Groups("read:proposalRequest")
     */
    private ?string $duration;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ProviderType", inversedBy="customerRequests")
     * @ORM\JoinTable(name="requests_provider_types",
     *      joinColumns={@ORM\JoinColumn(name="customer_request_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="provider_type_id", referencedColumnName="id")}
     *      )
     * @Assert\NotNull
     * @Assert\NotBlank
     * @Groups("read:proposalRequest")
     * @Groups("read:customerRequest")
     */
    private $providerTypes;

    /**
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="customerRequests", cascade={"remove"})
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     * @Assert\NotNull
     * @Assert\NotBlank
     */
    private $customer;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @ApiProperty(identifier=true)
     */
    private $validationToken;

    /**
     * @ORM\Column(type="datetime")
     */
    private $validationTokenExpireAt;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     * @Groups("read:proposalRequest")
     * @Groups("read:customerRequest")
     */
    private bool $validated;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $trackingNumber;

    /**
     * @ORM\ManyToOne(targetEntity="RequestState")
     * @ORM\JoinColumn(name="state_id", referencedColumnName="id")
     * @Groups("read:proposalRequest")
     * @Groups("read:customerRequest")
     */
    private $requestState;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default": 0})
     */
    private bool $archived;

    /**
     * @ORM\OneToMany(targetEntity="ProposalRequests", mappedBy="customerRequest")
     */
    private $proposalCustomerRequests;

    public function __construct()
    {
        parent::__construct();
        $this->setValidated(false);
        $this->setValidationTokenExpireAt(new \DateTime('NOW + 24 hours'));
        $stringHelper = new StringHelper();
        $this->setValidationToken($stringHelper->generateRandomString());
        $this->providerTypes = new ArrayCollection();
        $this->proposalCustomerRequests = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getPlace(): ?City
    {
        return $this->place;
    }

    public function setPlace(?City $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getPeopleNumber(): ?int
    {
        return $this->peopleNumber;
    }

    public function setPeopleNumber(?int $peopleNumber): self
    {
        $this->peopleNumber = $peopleNumber;

        return $this;
    }

    public function getBudget(): ?string
    {
        return $this->budget;
    }

    public function setBudget(?string $budget): self
    {
        $this->budget = $budget;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function setDuration(?string $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getProviderTypes()
    {
        return $this->providerTypes;
    }

    public function setProviderTypes($providerTypes): self
    {
        $this->providerTypes = $providerTypes;

        return $this;
    }

    public function addProviderType(ProviderType $providerType)
    {
        $this->providerTypes[] = $providerType;
    }

    /**
     * @return mixed
     */
    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    /**
     * @param mixed $customer
     */
    public function setCustomer(?Customer $customer): void
    {
        $this->customer = $customer;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     */
    public function setTime($time): void
    {
        $this->time = $time;
    }

    /**
     * @return mixed
     */
    public function getValidationToken()
    {
        return $this->validationToken;
    }

    /**
     * @param mixed $validationToken
     */
    public function setValidationToken($validationToken): void
    {
        $this->validationToken = $validationToken;
    }

    /**
     * @return mixed
     */
    public function isValidated()
    {
        return $this->validated;
    }

    /**
     * @param mixed $validated
     */
    public function setValidated($validated): void
    {
        $this->validated = $validated;
    }

    /**
     * @return mixed
     */
    public function getValidationTokenExpireAt()
    {
        return $this->validationTokenExpireAt;
    }

    /**
     * @param mixed $validationTokenExpireAt
     */
    public function setValidationTokenExpireAt($validationTokenExpireAt): void
    {
        $this->validationTokenExpireAt = $validationTokenExpireAt;
    }

    /**
     * @return mixed
     */
    public function getTrackingNumber()
    {
        return $this->trackingNumber;
    }

    /**
     * @param mixed $trackingNumber
     */
    public function setTrackingNumber($trackingNumber): void
    {
        $this->trackingNumber = $trackingNumber;
    }

    /**
     * @return mixed
     */
    public function getRequestState()
    {
        return $this->requestState;
    }

    /**
     * @param mixed $requestState
     */
    public function setRequestState($requestState): void
    {
        $this->requestState = $requestState;
    }

    /**
     * @return bool
     */
    public function isArchived(): ?bool
    {
        return $this->archived;
    }

    /**
     * @param mixed $archived
     */
    public function setArchived($archived): void
    {
        $this->archived = $archived;
    }

    /**
     * @return mixed
     */
    public function getProposalCustomerRequests()
    {
        return $this->proposalCustomerRequests;
    }

    /**
     * @param mixed $proposalCustomerRequests
     */
    public function setProposalCustomerRequests($proposalCustomerRequests): void
    {
        $this->proposalCustomerRequests = $proposalCustomerRequests;
    }
}
