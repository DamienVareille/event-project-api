<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Filter\RequestStateFilter;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"read:proposalRequest"}},
 *     collectionOperations={
 *          "get" = {"security"="is_granted('view_proposal_collection', object)"},
 *     },
 *     itemOperations={
 *          "get"    = {"security"="is_granted('view_proposal_item', object)"},
 *          "delete" = {"security"="is_granted('ROLE_ADMIN')"},
 *          "put"    = {"security"="(is_granted('ROLE_USER') and (object.getProvider().getUser().getUsername() == user.getUsername())) or is_granted('ROLE_ADMIN')"},
 *     },
 * )
 * @ORM\Table(name="proposal_requests")
 * @ORM\Entity(repositoryClass="App\Repository\ProposalRequestsRepository")
 * @ApiFilter(RequestStateFilter::class, properties={"requestState.constantCode": "exact"})
 */
class ProposalRequests extends BaseEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $propositionSentByProvider;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isLocalProvider;

    /**
     * @ORM\ManyToOne(targetEntity="Provider", inversedBy="proposalCustomerRequests")
     * @ORM\JoinColumn(name="provider_id", referencedColumnName="id")
     */
    public $provider;

    /**
     * @ORM\ManyToOne(targetEntity="CustomerRequest", inversedBy="proposalCustomerRequests")
     * @ORM\JoinColumn(name="customer_request_id", referencedColumnName="id")
     * @ApiSubresource
     * @Groups("read:proposalRequest")
     */
    private $customerRequest;

    /**
     * @ORM\ManyToOne(targetEntity="RequestState")
     * @ORM\JoinColumn(name="request_state_id", referencedColumnName="id")
     * @Groups("read:proposalRequest")
     */
    private $requestState;

    public function __construct()
    {
        parent::__construct();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getPropositionSentByProvider(): ?bool
    {
        return $this->propositionSentByProvider;
    }

    public function setPropositionSentByProvider(?bool $propositionSentByProvider): self
    {
        $this->propositionSentByProvider = $propositionSentByProvider;

        return $this;
    }

    public function getIsLocalProvider(): ?bool
    {
        return $this->isLocalProvider;
    }

    public function setIsLocalProvider(bool $isLocalProvider): self
    {
        $this->isLocalProvider = $isLocalProvider;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param mixed $provider
     */
    public function setProvider($provider): void
    {
        $this->provider = $provider;
    }

    /**
     * @return mixed
     */
    public function getCustomerRequest()
    {
        return $this->customerRequest;
    }

    /**
     * @param mixed $customerRequest
     */
    public function setCustomerRequest($customerRequest): void
    {
        $this->customerRequest = $customerRequest;
    }

    /**
     * @return mixed
     */
    public function getRequestState()
    {
        return $this->requestState;
    }

    /**
     * @param mixed $requestState
     */
    public function setRequestState($requestState): void
    {
        $this->requestState = $requestState;
    }
}
