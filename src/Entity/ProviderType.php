<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * ProviderType
 * @ApiResource(
 *     collectionOperations={
 *          "get",
 *          "post" = {"security"="is_granted('ROLE_ADMIN')"}
 *     },
 *     itemOperations={
 *          "get",
 *          "delete" = {"security"="is_granted('ROLE_ADMIN')"},
 *          "put"    = {"security"="is_granted('ROLE_ADMIN')"},
 *     },
 * )
 * @ApiFilter(SearchFilter::class, properties={"type": "start"})
 * @ApiFilter(OrderFilter::class, properties={"type"}, arguments={"orderParameterName"="order"})
 * @ORM\Table(name="provider_type")
 * @ORM\Entity
 */
class ProviderType extends BaseEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups("provider")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=150, nullable=false)
     * @Groups("read:provider")
     * @Groups("read:proposalRequest")
     * @Groups("read:customerRequest")
     */
    private $type;

    /**
     * @var boolean
     *
     * @ORM\Column(name="available", type="boolean")
     */
    private $available = true;

    /**
     * @ORM\ManyToMany(targetEntity="CustomerRequest", mappedBy="providerTypes")
     */
    private $customerRequests;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Provider", mappedBy="providerTypes")
     */
    private $providers;

    public function __construct()
    {
        parent::__construct();
        $this->customerRequests = new ArrayCollection();
        $this->providers        = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAvailable(): bool
    {
        return $this->available;
    }

    /**
     * @param bool $available
     */
    public function setAvailable(bool $available): void
    {
        $this->available = $available;
    }

    /**
     * @return mixed
     */
    public function getCustomerRequests()
    {
        return $this->customerRequests;
    }

    /**
     * @param mixed $customerRequests
     */
    public function setCustomerRequests($customerRequests): void
    {
        $this->customerRequests = $customerRequests;
    }

    /**
     * @return mixed
     */
    public function getProviders()
    {
        return $this->providers;
    }

    /**
     * @param mixed $providers
     */
    public function setProviders($providers): void
    {
        $this->providers = $providers;
    }

    public function __toString()
    {
        return $this->type;
    }


}
