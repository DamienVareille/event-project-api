<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\User;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Customer
 * @ApiResource(
 *     denormalizationContext={"groups"={"write:customer"}},
 *     collectionOperations={
 *          "get"  = {"security"="is_granted('ROLE_USER')"},
 *          "post"
 *     },
 *     itemOperations={
 *          "get"    = {"security"="is_granted('ROLE_ADMIN')"},
 *          "delete" = {"security"="is_granted('ROLE_ADMIN')"},
 *          "put"    = {"security"="is_granted('ROLE_ADMIN')"}
 *     },
 *
 * )
 * @ORM\Table(name="customer")
 * @ORM\Entity
 */
class Customer extends BaseEntity
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("write:customer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups("write:customer")
     * @Assert\Length(max=50)
     * @Assert\NotNull
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups("write:customer")
     * @Assert\Length(max=50)
     * @Assert\NotNull
     * @Assert\NotBlank
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Groups("write:customer")
     * @Assert\Length(max=50)
     */
    private $socialReason;

    /**
     * @ORM\Column(type="string", length=180)
     * @Groups("write:customer")
     * @Assert\Email
     * @Assert\NotNull
     * @Assert\NotBlank
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=12)
     * @Groups("write:customer")
     * @Assert\Regex("/(0|\\+33|0033)[1-9][0-9]{8}/")
     * @Assert\NotNull
     * @Assert\NotBlank
     */
    private $phoneNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="street_number", type="string", length=6, nullable=false)
     * @Groups("write:customer")
     * @Assert\Length(max=6)
     * @Assert\NotNull
     * @Assert\NotBlank
     */
    private $streetNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="street_name", type="string", length=255, nullable=false)
     * @Groups("write:customer")
     * @Assert\NotNull
     * @Assert\NotBlank
     */
    private $streetName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="additional_address", type="string", length=255, nullable=true)
     * @Groups("write:customer")
     */
    private $additionalAddress;

    /**
     * @ORM\ManyToOne(targetEntity="City")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", nullable=false)
     * @Groups("write:customer")
     * @Assert\NotNull
     * @Assert\NotBlank
     */
    private $city;

    /**
     * @ORM\OneToMany(targetEntity="CustomerRequest", mappedBy="customer")
     * @Groups("write:customer")
     * @ApiSubresource
     */
    private $customerRequests;

    public function __construct()
    {
        parent::__construct();
        $this->customerRequests = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param mixed $phoneNumber
     */
    public function setPhoneNumber($phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }


    /**
     * @return mixed
     */
    public function getSocialReason()
    {
        return $this->socialReason;
    }

    /**
     * @param mixed $socialReason
     */
    public function setSocialReason($socialReason): void
    {
        $this->socialReason = $socialReason;
    }

    /**
     * @return mixed
     */
    public function getCustomerRequests()
    {
        return $this->customerRequests;
    }

    /**
     * @param mixed $customerRequest
     */
    public function setCustomerRequests($customerRequest): void
    {
        $this->customerRequests = $customerRequest;
    }

    /**
     * @return string
     */
    public function getStreetNumber(): string
    {
        return $this->streetNumber;
    }

    /**
     * @param string $streetNumber
     */
    public function setStreetNumber(string $streetNumber): void
    {
        $this->streetNumber = $streetNumber;
    }

    /**
     * @return string
     */
    public function getStreetName(): string
    {
        return $this->streetName;
    }

    /**
     * @param string $streetName
     */
    public function setStreetName(string $streetName): void
    {
        $this->streetName = $streetName;
    }

    /**
     * @return string|null
     */
    public function getAdditionalAddress(): ?string
    {
        return $this->additionalAddress;
    }

    /**
     * @param string|null $additionalAddress
     */
    public function setAdditionalAddress(?string $additionalAddress): void
    {
        $this->additionalAddress = $additionalAddress;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city): void
    {
        $this->city = $city;
    }

    public function __toString()
    {
        return $this->getName().' '.$this->getSurname();
    }
}
