<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     denormalizationContext={"groups"={"write:provider"}},
 *     normalizationContext={"groups"={"read:provider"}},
 *     collectionOperations={
 *          "get" = {"security"="is_granted('ROLE_ADMIN')"},
 *          "post"
 *     },
 *     itemOperations={
 *          "get"    = {"security"="is_granted('ROLE_USER') and object.user == user"},
 *          "delete" = {"security"="is_granted('ROLE_ADMIN')"},
 *          "put"    = {"security"="is_granted('ROLE_PARTIAL_USER')"},
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\ProviderRepository")
 * @ApiFilter(SearchFilter::class, properties={"city.id": "exact", "providerType.id": "exact"})
 */
class Provider extends BaseEntity
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("write:provider")
     * @Groups("read:provider")
     * @Assert\NotNull
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("write:provider")
     * @Groups("read:provider")
     * @Assert\NotNull
     * @Assert\NotBlank
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("write:provider")
     * @Groups("read:provider")
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Groups("write:provider")
     * @Groups("read:provider")
     * @Assert\Regex("/(0|\\+33|0033)[1-9][0-9]{8}/")
     */
    private $phoneNumber;

    /**
     * @ORM\OneToOne(targetEntity="User", inversedBy="provider", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     * @Groups("write:provider")
     * @Groups("read:provider")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="City", cascade={"remove"})
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", nullable = true)
     * @Groups("write:provider")
     * @Groups("read:provider")
     */
    private $city;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ProviderType", inversedBy="providers")
     * @ORM\JoinTable(name="providers_types",
     *      joinColumns={@ORM\JoinColumn(name="provider_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="provider_type_id", referencedColumnName="id")}
     *      )
     * @Groups("write:provider")
     * @Groups("read:provider")
     */
    private $providerTypes;

    /**
     * @ORM\OneToMany(targetEntity="ProposalRequests", mappedBy="provider")
     * @Groups("read:provider")
     */
    private $proposalCustomerRequests;

    /**
     * Rayon d'action du prestataire (en kms)
     * @ORM\Column(type="integer", nullable=true)
     * @Groups("write:provider")
     * @Groups("read:provider")
     */
    private $actionRange;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("provider")
     * @Groups("read:provider")
     */
    private $profilePicture;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups("provider")
     * @Groups("read:provider")
     */
    private $description;

    public function __construct()
    {
        parent::__construct();
        $this->providerTypes = new ArrayCollection();
        $this->proposalCustomerRequests = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phone_number): self
    {
        $this->phoneNumber = $phone_number;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city): void
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getActionRange()
    {
        return $this->actionRange;
    }

    /**
     * @param mixed $actionRange
     */
    public function setActionRange($actionRange): void
    {
        $this->actionRange = $actionRange;
    }

    /**
     * @return mixed
     */
    public function getProfilePicture()
    {
        return $this->profilePicture;
    }

    /**
     * @param mixed $profilePicture
     */
    public function setProfilePicture($profilePicture): void
    {
        $this->profilePicture = $profilePicture;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getProposalCustomerRequests()
    {
        return $this->proposalCustomerRequests;
    }

    /**
     * @param mixed $proposalCustomerRequests
     */
    public function setProposalCustomerRequests($proposalCustomerRequests): void
    {
        $this->proposalCustomerRequests = $proposalCustomerRequests;
    }

    /**
     * @return mixed
     */
    public function getProviderTypes()
    {
        return $this->providerTypes;
    }

    /**
     * @param mixed $providerTypes
     */
    public function setProviderTypes($providerTypes): void
    {
        $this->providerTypes = $providerTypes;
    }

    public function addProviderType($provider_type): void
    {
        $this->providerTypes->add($provider_type);
    }
}
