<?php


namespace App\Tests\RequestDispatcher;

use App\DataFixtures\AddPendingCustomerRequestFixtures;
use App\Entity\CustomerRequest;
use App\Entity\RequestState;
use App\RequestDispatcher\Dispatcher;
use App\RequestDispatcher\Logger;
use App\RequestDispatcher\ProvidersFinder;
use App\Tests\FixtureAwareTestCase;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManagerInterface;

class DispatcherWithOnePendingRequestTest extends FixtureAwareTestCase
{
    private EntityManagerInterface $em;

    private Logger $logger;

    private ProvidersFinder $finder;

    protected function setUp()
    {
        $kernel = static::bootKernel();
        $this->em = $kernel->getContainer()->get('doctrine')->getManager();
        $this->logger = $this->createMock(Logger::class);
        $this->finder = $this->createMock(ProvidersFinder::class);

        $this->addFixture(new AddPendingCustomerRequestFixtures());
        $this->executeFixtures();
    }

    public function testDispatcherWithOnePendingRequest(): void
    {
        $dispatcher = new Dispatcher($this->em, $this->logger, $this->finder);
        $this->assertEquals(Dispatcher::PENDING_REQUESTS_DISPATCHED, $dispatcher->dispatch());
    }

    public function tearDown()
    {
        /** @var CustomerRequest $cr */
        $cr = $this->em->getRepository(CustomerRequest::class)->findAll()[0];
        $notValidatedState = $this->em->getRepository(RequestState::class)->findOneBy(['constantCode' => RequestState::NOT_VALIDATED]);
        $cr->setRequestState($notValidatedState);
        $cr->setArchived(false);
        $cr->setValidated(true);
        $this->em->persist($cr);
        $this->em->flush();
    }
}