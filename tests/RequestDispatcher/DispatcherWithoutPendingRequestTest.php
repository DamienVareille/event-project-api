<?php


namespace App\Tests\RequestDispatcher;


use App\DataFixtures\AddPendingCustomerRequestFixtures;
use App\Entity\CustomerRequest;
use App\Entity\RequestState;
use App\RequestDispatcher\Dispatcher;
use App\RequestDispatcher\Logger;
use App\RequestDispatcher\ProvidersFinder;
use App\Tests\FixtureAwareTestCase;
use Doctrine\ORM\EntityManagerInterface;

class DispatcherWithoutPendingRequestTest extends FixtureAwareTestCase
{
    private EntityManagerInterface $em;

    private Logger $logger;

    private ProvidersFinder $finder;

    protected function setUp()
    {
        $kernel = static::bootKernel();
        $this->em = $kernel->getContainer()->get('doctrine')->getManager();
        $this->logger = $this->createMock(Logger::class);
        $this->finder = $this->createMock(ProvidersFinder::class);
    }

    public function testDispatcherWithoutPendingRequests(): void
    {
        /** @var Dispatcher $dispatcher */
        $dispatcher = new Dispatcher($this->em, $this->logger, $this->finder);
        $this->assertEquals(Dispatcher::NO_PENDING_REQUEST, $dispatcher->dispatch());
    }
}