<?php


namespace App\Tests\Helper;

use App\Helpers\StringHelper;
use PHPUnit\Framework\TestCase;

class StringHelperTest extends TestCase
{

    public function testGenerateRandomString(): void
    {
        $stringHelper = new StringHelper();

        $string = $stringHelper->generateRandomString();

        $this->assertEquals(strlen($string), 60);

        $string = $stringHelper->generateRandomString(80);

        $this->assertEquals(strlen($string), 80);
    }

}