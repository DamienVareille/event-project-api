<?php


namespace App\Tests\bundle;


use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\User;
use Doctrine\ORM\EntityManager;

class CustomApiTestCase extends ApiTestCase
{
    const BASE_URL = 'https://localhost';

    protected function findFirstIri(string $resourceClass): ?string
    {
        if (null === static::$container) {
            throw new \RuntimeException(sprintf('The container is not available. You must call "bootKernel()" or "createClient()" before calling "%s".', __METHOD__));
        }

        if (
            (
                !static::$container->has('doctrine') ||
                null === $objectManager = static::$container->get('doctrine')->getManagerForClass($resourceClass)
            ) &&
            (
                !static::$container->has('doctrine_mongodb') ||
                null === $objectManager = static::$container->get('doctrine_mongodb')->getManagerForClass($resourceClass)
            )
        ) {
            throw new \RuntimeException(sprintf('"%s" only supports classes managed by Doctrine ORM or Doctrine MongoDB ODM. Override this method to implement your own retrieval logic if you don\'t use those libraries.', __METHOD__));
        }

        $item = $objectManager->getRepository($resourceClass)->findOneBy([]);
        if (null === $item) {
            return null;
        }

        return self::BASE_URL.static::$container->get('api_platform.iri_converter')->getIriFromItem($item);
    }

    protected function getJWTTokenWithRole($role): ?string
    {
        $client = static::createClient();

        if (null === static::$container) {
            throw new \RuntimeException(sprintf('The container is not available. You must call "bootKernel()" or "createClient()" before calling "%s".', __METHOD__));
        }

        if (
            (
                !static::$container->has('doctrine') ||
                null === $objectManager = static::$container->get('doctrine')->getManagerForClass(User::class)
            ) &&
            (
                !static::$container->has('doctrine_mongodb') ||
                null === $objectManager = static::$container->get('doctrine_mongodb')->getManagerForClass(User::class)
            )
        ) {
            throw new \RuntimeException(sprintf('"%s" only supports classes managed by Doctrine ORM or Doctrine MongoDB ODM. Override this method to implement your own retrieval logic if you don\'t use those libraries.', __METHOD__));
        }

        switch ($role)
        {
            case 'user':
                $finalRole = '%ROLE_USER%';
                break;
            case 'admin':
                $finalRole = '%ROLE_ADMIN%';
                break;
            default:
                $finalRole = '';
        }

        $user = $this->getUserFromRole($finalRole);

        if (null === $user) {
            return null;
        }

        $response = $client->request('POST', self::BASE_URL.'/authentication_token', ['json' => ['email' => $user->getUsername(), 'password' => 'test']]);

        return $response->toArray()['token'];
    }

    protected function getUserFromRole(string $role): ?User
    {
        $client = static::createClient();

        if (null === static::$container) {
            throw new \RuntimeException(sprintf('The container is not available. You must call "bootKernel()" or "createClient()" before calling "%s".', __METHOD__));
        }

        if (
            (
                !static::$container->has('doctrine') ||
                null === $objectManager = static::$container->get('doctrine')->getManagerForClass(User::class)
            ) &&
            (
                !static::$container->has('doctrine_mongodb') ||
                null === $objectManager = static::$container->get('doctrine_mongodb')->getManagerForClass(User::class)
            )
        ) {
            throw new \RuntimeException(sprintf('"%s" only supports classes managed by Doctrine ORM or Doctrine MongoDB ODM. Override this method to implement your own retrieval logic if you don\'t use those libraries.', __METHOD__));
        }

        $qb = $objectManager->createQueryBuilder();

        return $qb->select('u')
            ->from(User::class, 'u')
            ->where('u.roles LIKE :roles')
            ->setParameter('roles', $role)
            ->getQuery()
            ->getOneOrNullResult();
    }

}