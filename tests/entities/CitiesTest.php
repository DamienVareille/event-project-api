<?php

namespace App\Tests;

use App\Entity\City;
use App\Tests\bundle\CustomApiTestCase;

class CitiesTest extends CustomApiTestCase
{
    /*
     * NO AUTH TESTING
     */

    public function testGetCollectionNoAuth(): void
    {
        $response = static::createClient()->request('GET', self::BASE_URL.'/api/cities');

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertCount(30, $response->toArray()['hydra:member']);
        $this->assertMatchesResourceCollectionJsonSchema(City::class);
    }

    public function testGetItemNoAuth(): void
    {
        $client = static::createClient();
        $iri = static::findFirstIri(City::class);

        $client->request('GET', $iri);

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertMatchesResourceItemJsonSchema(City::class);
    }

    public function testPostNoAuth(): void
    {
        static::createClient()->request('POST', self::BASE_URL.'/api/cities', ['json' => [
            'city_insee_code' => '00000',
            'city_nom_reel' => 'Une ville',
            'city_code_postal' => '00000',
            'city_nom' => 'VILLE',
        ]]);

        $this->assertResponseStatusCodeSame(401, "JWT Token not found");
        $this->assertResponseHeaderSame('content-type', 'application/json');
    }

    public function testPutNoAuth(): void
    {
        $client = static::createClient();
        $city = static::findFirstIri(City::class);

        $client->request('PUT', $city, ['json' => [
            'city_insee_code' => '00000',
            'city_nom_reel' => 'Une ville',
        ]]);

        $this->assertResponseStatusCodeSame(401, "JWT Token not found");
        $this->assertResponseHeaderSame('content-type', 'application/json');
    }

    /*
     *  ROLE_USER TESTING
     */

    public function testGetCollectionRoleUser(): void
    {
        $jwt = static::getJWTTokenWithRole('user');

        $client = static::createClient();

        $response = $client->request('GET', self::BASE_URL.'/api/cities', ['auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertMatchesResourceCollectionJsonSchema(City::class);
        $this->assertCount(30, $response->toArray()['hydra:member']);
    }

    public function testGetItemRoleUser(): void
    {
        $jwt = static::getJWTTokenWithRole('user');

        $client = static::createClient();

        $iri = static::findFirstIri(City::class);

        $client->request('GET', $iri, ['auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertMatchesResourceCollectionJsonSchema(City::class);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
    }

    public function testPostRoleUser(): void
    {
        $jwt = static::getJWTTokenWithRole('user');

        $client = static::createClient();
        $response = $client->request('POST', self::BASE_URL.'/api/cities', ['json' => [

          "cityNom" => "string",
          "cityNomReel" => "string",
          "cityCodePostal" => "string",
          "cityInseeCode" => "string"

        ], 'auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(403);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');

    }

    public function testPutRoleUser(): void
    {
        $jwt = static::getJWTTokenWithRole('user');
        $client = static::createClient();

        $client->request('PUT', static::findFirstIri(City::class), ['json' => [
            'name' => 'test',
        ], 'auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(403);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
    }

    /*
     *  ROLE_ADMIN TESTING
     */

    public function testGetCollectionRoleAdmin(): void
    {
        $jwt = static::getJWTTokenWithRole('admin');

        $client = static::createClient();

        $response = $client->request('GET', self::BASE_URL.'/api/cities', ['auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertMatchesResourceCollectionJsonSchema(City::class);
        $this->assertCount(30, $response->toArray()['hydra:member']);
    }

    public function testGetItemRoleAdmin(): void
    {
        $jwt = static::getJWTTokenWithRole('admin');

        $client = static::createClient();

        $iri = static::findFirstIri(City::class);

        $client->request('GET', $iri, ['auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertMatchesJsonSchema('
            {
              "@context": "/api/contexts/City",
              "@id": "/api/cities/60212",
              "@type": "City",
              "id": 60212,
              "cityNom": "string",
              "cityNomReel": "string",
              "cityCodePostal": "string",
              "cityInseeCode": "string"
            }
        ');
    }

    public function testPostRoleAdmin(): void
    {
        $jwt = static::getJWTTokenWithRole('admin');

        $client = static::createClient();
        $response = $client->request('POST', self::BASE_URL.'/api/cities', ['json' => [
            "cityNom" => "string",
            "cityNomReel" => "string",
            "cityCodePostal" => "string",
            "cityInseeCode" => "string"
        ], 'auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertRegExp('~^/api/cities/\d+$~', $response->toArray()['@id']);
        $this->assertMatchesResourceItemJsonSchema(City::class);
        $this->assertMatchesJsonSchema('
            {
              "@context": "/api/contexts/City",
              "@id": "/api/cities/60212",
              "@type": "City",
              "id": 60212,
              "cityNom": "string",
              "cityNomReel": "string",
              "cityCodePostal": "string",
              "cityInseeCode": "string"
            }
        ');
    }

    public function testPutRoleAdmin(): void
    {
        $jwt = static::getJWTTokenWithRole('admin');
        $client = static::createClient();

        $client->request('PUT', static::findFirstIri(City::class), ['json' => [
            'cityNom' => 'test',
        ], 'auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
    }

}