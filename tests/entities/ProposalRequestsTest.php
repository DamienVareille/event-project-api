<?php


namespace App\Tests\entities;


use App\Entity\Customer;
use App\Entity\CustomerRequest;
use App\Entity\ProposalRequests;
use App\Entity\Provider;
use App\Tests\bundle\CustomApiTestCase;

class ProposalRequestsTest extends CustomApiTestCase
{

    /*
     * NO AUTH TESTING
     */

    public function testGetCollectionNoAuth(): void
    {
        static::createClient()->request('GET', self::BASE_URL.'/api/proposal_requests');

        $this->assertResponseStatusCodeSame(401, "JWT Token not found");
        $this->assertResponseHeaderSame('content-type', 'application/json');
    }

    public function testGetItemNoAuth(): void
    {
        $client = static::createClient();
        $iri = static::findFirstIri(ProposalRequests::class);

        $client->request('GET', $iri);

        $this->assertResponseStatusCodeSame(401, "JWT Token not found");
        $this->assertResponseHeaderSame('content-type', 'application/json');
    }

    public function testPostNoAuth(): void
    {
        $client = static::createClient();
        $provider = static::findFirstIri(Provider::class);
        $req = static::findFirstIri(CustomerRequest::class);

        $client->request('POST', self::BASE_URL.'/api/proposal_requests', ['json' => [
            "propositionSentByProvider" => true,
            "isLocalProvider" => true,
            "provider" => $provider,
            "customerRequest" => $req
        ]]);

        $this->assertResponseStatusCodeSame(405);
    }

    public function testPutNoAuth(): void
    {
        static::createClient()->request('PUT', static::findFirstIri(ProposalRequests::class), ['json' => [
            'name' => 'test',
        ]]);

        $this->assertResponseStatusCodeSame(401);
    }

    /*
     *  ROLE_USER TESTING
     */

    public function testGetCollectionRoleUser(): void
    {
        $jwt = static::getJWTTokenWithRole('user');

        $client = static::createClient();

        $response = $client->request('GET', self::BASE_URL.'/api/proposal_requests', ['auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertMatchesResourceCollectionJsonSchema(ProposalRequests::class);
        $this->assertEquals(2, \count($response->toArray()['hydra:member']));
    }

    public function testGetCollectionWithPendingStateRoleUser(): void
    {
        $jwt = static::getJWTTokenWithRole('user');

        $client = static::createClient();

        $response = $client->request('GET', self::BASE_URL.'/api/proposal_requests?requestState.constantCode=PENDING', ['auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertMatchesResourceCollectionJsonSchema(ProposalRequests::class);
        $this->assertEquals(1, \count($response->toArray()['hydra:member']));
    }
/*
    public function testGetItemRoleUser(): void
    {
        $jwt = static::getJWTTokenWithRole('user');

        $client = static::createClient();

        $iri = static::findFirstIri(Customer::class);

        $client->request('GET', $iri, ['auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(403);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
    }
*/
    public function testPostRoleUser(): void
    {
        $client = static::createClient();
        $provider = static::findFirstIri(Provider::class);
        $req = static::findFirstIri(CustomerRequest::class);
        $client->request('POST', self::BASE_URL.'/api/proposal_requests', ['json' => [
            "propositionSentByProvider" => true,
            "isLocalProvider" => true,
            "provider" => $provider,
            "customerRequest" => $req
        ]]);

        $this->assertResponseStatusCodeSame(405);
    }

    public function testPutRoleUser(): void
    {
        $jwt = static::getJWTTokenWithRole('user');
        $client = static::createClient();

        $client->request('PUT', static::findFirstIri(ProposalRequests::class), ['json' => [
            'name' => 'test',
        ], 'auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
    }

    /*
     *  ROLE_ADMIN TESTING
     */

    public function testGetCollectionRoleAdmin(): void
    {
        $jwt = static::getJWTTokenWithRole('admin');

        $client = static::createClient();

        $response = $client->request('GET', self::BASE_URL.'/api/proposal_requests', ['auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertMatchesResourceCollectionJsonSchema(ProposalRequests::class);
        $this->assertCount(2, $response->toArray()['hydra:member']);
    }
    /*
        public function testGetItemRoleAdmin(): void
        {
            $jwt = static::getJWTTokenWithRole('admin');

            $client = static::createClient();

            $iri = static::findFirstIri(Customer::class);

            $client->request('GET', $iri, ['auth_bearer' => $jwt]);

            $this->assertResponseStatusCodeSame(200);
            $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
            $this->assertMatchesJsonSchema('
                {
                  "@context": "/api/contexts/Customer",
                  "@id": "/api/customers/1715",
                  "@type": "Customer",
                  "id": 1715,
                  "name": "Guyot",
                  "surname": "Daniel",
                  "email": "william77@dbmail.com",
                  "phoneNumber": "0555028881",
                  "socialReason": "Lemonnier S.A.",
                  "customerRequests": [
                  ],
                  "streetNumber": "12",
                  "streetName": "rue de Gautier",
                  "additionalAddress": "Chambre 570",
                  "city": "/api/cities/1595"
                }
            ');
        }
    */
    public function testPostRoleAdmin(): void
    {
        $client = static::createClient();
        $provider = static::findFirstIri(Provider::class);
        $req = static::findFirstIri(CustomerRequest::class);
        $client->request('POST', self::BASE_URL.'/api/proposal_requests', ['json' => [
            "propositionSentByProvider" => true,
            "isLocalProvider" => true,
            "provider" => $provider,
            "customerRequest" => $req
        ]]);

        $this->assertResponseStatusCodeSame(405);
    }

    public function testPutRoleAdmin(): void
    {
        $jwt = static::getJWTTokenWithRole('admin');
        $client = static::createClient();

        $client->request('PUT', static::findFirstIri(ProposalRequests::class), ['json' => [
            'name' => 'test',
        ], 'auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
    }

}