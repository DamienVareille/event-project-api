<?php


namespace App\Tests\bundle;


use App\Entity\City;
use App\Entity\Customer;
use App\Entity\CustomerRequest;
use App\Entity\CustomerRequestState;
use App\Entity\ProviderType;
use App\Entity\RequestState;

class CustomerRequestsTest extends CustomApiTestCase
{
    /*
     * NO AUTH TESTING
     */

    public function testGetCollectionNoAuth(): void
    {
        static::createClient()->request('GET', self::BASE_URL.'/api/customer_requests');

        $this->assertResponseStatusCodeSame(401, "JWT Token not found");
        $this->assertResponseHeaderSame('content-type', 'application/json');
    }

    public function testGetItemNoAuth(): void
    {
        $client = static::createClient();
        $iri = static::findFirstIri(CustomerRequest::class);

        $client->request('GET', $iri);

        $this->assertResponseStatusCodeSame(401, "JWT Token not found");
        $this->assertResponseHeaderSame('content-type', 'application/json');
    }

    public function testPostNoAuth(): void
    {
        $client = static::createClient();
        $city = static::findFirstIri(City::class);
        $providerType = static::findFirstIri(ProviderType::class);
        $customer = static::findFirstIri(Customer::class);
        $state = static::findFirstIri(RequestState::class);
        $response = $client->request('POST', self::BASE_URL.'/api/customer_requests', ['json' => [
          "date"=> "2020-03-11T18:15:40.237Z",
          "place"=> $city,
          "peopleNumber"=> 0,
          "budget"=> "string",
          "description"=> "string",
          "duration"=> "string",
          "providerTypes"=> [
              $providerType
            ],
          "customer"=> $customer,
          "time"=> "2020-03-11T18:15:40.237Z",
          "validationToken"=> "string",
          "validated"=> true,
          "validationTokenExpireAt"=> "2020-03-11T18:15:40.237Z",
          "trackingNumber"=> "string",
          "state"=> $state,
          "archived"=> true,
        ]]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertRegExp('~^/api/customer_requests/.~', $response->toArray()['@id']);
        $this->assertMatchesResourceItemJsonSchema(CustomerRequest::class);
    }

    public function testPutNoAuth(): void
    {
        static::createClient()->request('PUT', static::findFirstIri(CustomerRequest::class), ['json' => [
            'name' => 'test',
        ]]);

        $this->assertResponseStatusCodeSame(200);
    }

    /*
     *  ROLE_USER TESTING
     */

    public function testGetCollectionRoleUser(): void
    {
        $jwt = static::getJWTTokenWithRole('user');

        $client = static::createClient();

        $response = $client->request('GET', self::BASE_URL.'/api/customer_requests', ['auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertMatchesResourceCollectionJsonSchema(CustomerRequest::class);
        $this->assertCount(2, $response->toArray()['hydra:member']);
    }

    public function testGetItemRoleUser(): void
    {
        $jwt = static::getJWTTokenWithRole('user');

        $client = static::createClient();

        $iri = static::findFirstIri(CustomerRequest::class);

        $client->request('GET', $iri, ['auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertMatchesJsonSchema('
            {
              "@context": "/api/contexts/CustomerRequest",
              "@id": "/api/customer_requests/238",
              "@type": "CustomerRequest",
              "id": 238,
              "date": "1970-10-01T00:00:00+01:00",
              "place": "/api/cities/36231",
              "peopleNumber": 107,
              "budget": "450€",
              "description": "Sit quia ipsum voluptatum eligendi eum pariatur eos. Enim eum est voluptas nemo velit dicta. Aut qui repellendus natus dolorem.",
              "duration": "2h",
              "providerTypes": [
                "/api/provider_types/1085"
              ],
              "customer": "/api/customers/1749",
              "time": "1970-01-01T13:35:40+01:00",
              "validationToken": "wG2EIG2pEAh4mStS9EiwPT5VtkysKiFY27hfF1bqSKiearMPXbv3X0AedRtt",
              "validated": true,
              "validationTokenExpireAt": "2020-03-09T23:57:35+01:00",
              "trackingNumber": "486R2V",
              "state": "/api/customer_request_states/66",
              "archived": null,
              "proposalCustomerRequests": [
                "/api/proposal_requests/2"
              ]
            }
        ');
    }

    public function testPostRoleUser(): void
    {
        $jwt = static::getJWTTokenWithRole('user');
        $city = static::findFirstIri(City::class);
        $providerType = static::findFirstIri(ProviderType::class);
        $customer = static::findFirstIri(Customer::class);
        $state = static::findFirstIri(RequestState::class);
        $client = static::createClient();
        $response = $client->request('POST', self::BASE_URL.'/api/customer_requests', ['json' => [
            "date"=> "2020-03-11T18:15:40.237Z",
            "place"=> $city,
            "peopleNumber"=> 0,
            "budget"=> "string",
            "description"=> "string",
            "duration"=> "string",
            "providerTypes"=> [
                $providerType
            ],
            "customer"=> $customer,
            "time"=> "2020-03-11T18:15:40.237Z",
            "validationToken"=> "string",
            "validated"=> true,
            "validationTokenExpireAt"=> "2020-03-11T18:15:40.237Z",
            "trackingNumber"=> "string",
            "state"=> $state,
            "archived"=> true,
        ], 'auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertRegExp('~^/api/customer_requests/.~', $response->toArray()['@id']);
        $this->assertMatchesResourceItemJsonSchema(CustomerRequest::class);
        $this->assertMatchesJsonSchema('
            {
              "@context": "/api/contexts/CustomerRequest",
              "@id": "/api/customer_requests/238",
              "@type": "CustomerRequest",
              "id": 238,
              "date": "1970-10-01T00:00:00+01:00",
              "place": "/api/cities/36231",
              "peopleNumber": 107,
              "budget": "450€",
              "description": "Sit quia ipsum voluptatum eligendi eum pariatur eos. Enim eum est voluptas nemo velit dicta. Aut qui repellendus natus dolorem.",
              "duration": "2h",
              "providerTypes": [
                "/api/provider_types/1085"
              ],
              "customer": "/api/customers/1749",
              "time": "1970-01-01T13:35:40+01:00",
              "validationToken": "wG2EIG2pEAh4mStS9EiwPT5VtkysKiFY27hfF1bqSKiearMPXbv3X0AedRtt",
              "validated": true,
              "validationTokenExpireAt": "2020-03-09T23:57:35+01:00",
              "trackingNumber": "486R2V",
              "state": "/api/customer_request_states/66",
              "archived": null,
              "proposalCustomerRequests": [
                "/api/proposal_requests/2"
              ]
            }
        ');
    }

    public function testPutRoleUser(): void
    {
        $jwt = static::getJWTTokenWithRole('user');
        $client = static::createClient();

        $client->request('PUT', static::findFirstIri(CustomerRequest::class), ['json' => [
            'name' => 'test',
        ], 'auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
    }

    /*
     *  ROLE_ADMIN TESTING
     */

    public function testGetCollectionRoleAdmin(): void
    {
        $jwt = static::getJWTTokenWithRole('admin');

        $client = static::createClient();

        $response = $client->request('GET', self::BASE_URL.'/api/customer_requests', ['auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertMatchesResourceCollectionJsonSchema(CustomerRequest::class);
        $this->assertCount(3, $response->toArray()['hydra:member']);
    }

    public function testGetItemRoleAdmin(): void
    {
        $jwt = static::getJWTTokenWithRole('admin');

        $client = static::createClient();

        $iri = static::findFirstIri(CustomerRequest::class);

        $client->request('GET', $iri, ['auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertMatchesJsonSchema('
            {
              "@context": "/api/contexts/CustomerRequest",
              "@id": "/api/customer_requests/239",
              "@type": "CustomerRequest",
              "id": 239,
              "date": "1990-03-17T00:00:00+01:00",
              "place": "/api/cities/4177",
              "peopleNumber": 333,
              "budget": "450€",
              "description": "Dolor et dignissimos qui voluptatum aut architecto. Et perspiciatis illo rem molestiae pariatur autem.",
              "duration": "2h",
              "providerTypes": [],
              "customer": "/api/customers/1755",
              "time": "1970-01-01T21:00:11+01:00",
              "validationToken": "aSqqjKLcpJ9KHSOFFkD8aFrqekHvSODlVCQO8dtiUfeLN3m4lFLxcEly17po",
              "validated": true,
              "validationTokenExpireAt": "2020-03-09T23:57:35+01:00",
              "trackingNumber": "CPHFY4",
              "state": "/api/customer_request_states/68",
              "archived": null,
              "proposalCustomerRequests": []
            }
        ');
    }

    public function testPostRoleAdmin(): void
    {
        $jwt = static::getJWTTokenWithRole('admin');
        $city = static::findFirstIri(City::class);
        $providerType = static::findFirstIri(ProviderType::class);
        $customer = static::findFirstIri(Customer::class);
        $state = static::findFirstIri(RequestState::class);

        $client = static::createClient();
        $response = $client->request('POST', self::BASE_URL.'/api/customer_requests', ['json' => [
            "date"=> "2020-03-11T18:15:40.237Z",
            "place"=> $city,
            "peopleNumber"=> 0,
            "budget"=> "string",
            "description"=> "string",
            "duration"=> "string",
            "providerTypes"=> [
                $providerType
            ],
            "customer"=> $customer,
            "time"=> "2020-03-11T18:15:40.237Z",
            "validationToken"=> "string",
            "validated"=> true,
            "validationTokenExpireAt"=> "2020-03-11T18:15:40.237Z",
            "trackingNumber"=> "string",
            "state"=> $state,
            "archived"=> true,
        ], 'auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertRegExp('~^/api/customer_requests/.~', $response->toArray()['@id']);
        $this->assertMatchesResourceItemJsonSchema(CustomerRequest::class);
        $this->assertMatchesJsonSchema('
            {
              "@context": "/api/contexts/CustomerRequest",
              "@id": "/api/customer_requests/239",
              "@type": "CustomerRequest",
              "id": 239,
              "date": "1990-03-17T00:00:00+01:00",
              "place": "/api/cities/4177",
              "peopleNumber": 333,
              "budget": "450€",
              "description": "Dolor et dignissimos qui voluptatum aut architecto. Et perspiciatis illo rem molestiae pariatur autem.",
              "duration": "2h",
              "providerTypes": [],
              "customer": "/api/customers/1755",
              "time": "1970-01-01T21:00:11+01:00",
              "validationToken": "aSqqjKLcpJ9KHSOFFkD8aFrqekHvSODlVCQO8dtiUfeLN3m4lFLxcEly17po",
              "validated": true,
              "validationTokenExpireAt": "2020-03-09T23:57:35+01:00",
              "trackingNumber": "CPHFY4",
              "state": "/api/customer_request_states/68",
              "archived": null,
              "proposalCustomerRequests": []
            }
        ');
    }

    public function testPutRoleAdmin(): void
    {
        $jwt = static::getJWTTokenWithRole('admin');
        $client = static::createClient();

        $client->request('PUT', static::findFirstIri(CustomerRequest::class), ['json' => [
            'name' => 'test',
        ], 'auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
    }

}