<?php


namespace App\Tests\entities;


use App\Entity\RequestState;
use App\Tests\bundle\CustomApiTestCase;

class CustomerRequestStatesTest extends CustomApiTestCase
{

    /*
     * NO AUTH TESTING
     */

    public function testGetCollectionNoAuth(): void
    {
        static::createClient()->request('GET', self::BASE_URL.'/api/request_states');

        $this->assertResponseStatusCodeSame(401, "JWT Token not found");
        $this->assertResponseHeaderSame('content-type', 'application/json');
    }

    public function testGetItemNoAuth(): void
    {
        $client = static::createClient();
        $iri = static::findFirstIri(RequestState::class);

        $client->request('GET', $iri);

        $this->assertResponseStatusCodeSame(401, "JWT Token not found");
        $this->assertResponseHeaderSame('content-type', 'application/json');
    }

    public function testPostNoAuth(): void
    {
        static::createClient()->request('POST', self::BASE_URL.'/api/request_states', ['json' => [
            'state' => 'TEST',
            'code' => 'TEST',
            'state_name' => 'Test'
        ]]);

        $this->assertResponseStatusCodeSame(401, "JWT Token not found");
        $this->assertResponseHeaderSame('content-type', 'application/json');
    }

    public function testPutNoAuth(): void
    {
        $client = static::createClient();
        $state = static::findFirstIri(RequestState::class);

        $client->request('PUT', $state, ['json' => [
            'state' => 'TEST',
            'code' => 'TEST',
            'state_name' => 'Test'
        ]]);

        $this->assertResponseStatusCodeSame(401, "JWT Token not found");
        $this->assertResponseHeaderSame('content-type', 'application/json');
    }

    /*
     *  ROLE_USER TESTING
     */

    public function testGetCollectionRoleUser(): void
    {
        $jwt = static::getJWTTokenWithRole('user');

        $client = static::createClient();

        $response = $client->request('GET', self::BASE_URL.'/api/request_states', ['auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertMatchesResourceCollectionJsonSchema(RequestState::class);
    }

    public function testGetItemRoleUser(): void
    {
        $jwt = static::getJWTTokenWithRole('user');

        $client = static::createClient();

        $iri = static::findFirstIri(RequestState::class);

        $client->request('GET', $iri, ['auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertMatchesResourceCollectionJsonSchema(RequestState::class);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
    }

    public function testPostRoleUser(): void
    {
        $jwt = static::getJWTTokenWithRole('user');

        $client = static::createClient();
        $client->request('POST', self::BASE_URL.'/api/request_states', ['json' => [
            'state' => 'TEST',
            'code' => 'TEST',
            'state_name' => 'Test'
        ], 'auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(403);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');

    }

    public function testPutRoleUser(): void
    {
        $jwt = static::getJWTTokenWithRole('user');
        $client = static::createClient();

        $client->request('PUT', static::findFirstIri(RequestState::class), ['json' => [
            'state' => 'TEST',
            'code' => 'TEST',
            'state_name' => 'Test'
        ], 'auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(403);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
    }

    /*
     *  ROLE_ADMIN TESTING
     */

    public function testGetCollectionRoleAdmin(): void
    {
        $jwt = static::getJWTTokenWithRole('admin');

        $client = static::createClient();

        $response = $client->request('GET', self::BASE_URL.'/api/request_states', ['auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertMatchesResourceCollectionJsonSchema(RequestState::class);
    }

    public function testGetItemRoleAdmin(): void
    {
        $jwt = static::getJWTTokenWithRole('admin');

        $client = static::createClient();

        $iri = static::findFirstIri(RequestState::class);

        $client->request('GET', $iri, ['auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertMatchesJsonSchema('
            {
              "@context": "/api/contexts/RequestState",
              "@id": "/api/request_states/74",
              "@type": "RequestState",
              "id": 74,
              "state": "INVALID",
              "code": "1",
              "stateName": "Non validée"
            }
        ');
    }

    public function testPostRoleAdmin(): void
    {
        $jwt = static::getJWTTokenWithRole('admin');

        $client = static::createClient();
        $response = $client->request('POST', self::BASE_URL.'/api/request_states', ['json' => [
            'constantCode' => 'TEST',
            'code' => 'TEST',
            'name' => 'Test'
        ], 'auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertRegExp('~^/api/request_states/\d+$~', $response->toArray()['@id']);
        $this->assertMatchesResourceItemJsonSchema(RequestState::class);
        $this->assertMatchesJsonSchema('
            {
              "@context": "/api/contexts/RequestState",
              "@id": "/api/request_states/74",
              "@type": "RequestState",
              "id": 74,
              "constantCode": "INVALID",
              "code": "1",
              "name": "Non validée"
            }
        ');
    }

    public function testPutRoleAdmin(): void
    {
        $jwt = static::getJWTTokenWithRole('admin');
        $client = static::createClient();

        $client->request('PUT', static::findFirstIri(RequestState::class), ['json' => [
            'state' => 'TEST',
            'code' => 'TEST',
            'state_name' => 'Test'
        ], 'auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
    }

}