<?php


namespace App\Tests;


use App\Entity\City;
use App\Entity\Customer;
use App\Tests\bundle\CustomApiTestCase;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class CustomersTest extends CustomApiTestCase
{

    /*
     * NO AUTH TESTING
     */

    public function testGetCollectionNoAuth(): void
    {
        static::createClient()->request('GET', self::BASE_URL.'/api/customers');

        $this->assertResponseStatusCodeSame(401, "JWT Token not found");
        $this->assertResponseHeaderSame('content-type', 'application/json');
    }

    public function testGetItemNoAuth(): void
    {
        $client = static::createClient();
        $iri = static::findFirstIri(Customer::class);

        $client->request('GET', $iri);

        $this->assertResponseStatusCodeSame(401, "JWT Token not found");
        $this->assertResponseHeaderSame('content-type', 'application/json');
    }

    public function testPostNoAuth(): void
    {
        $client = static::createClient();
        $city = static::findFirstIri(City::class);
        $response = $client->request('POST', self::BASE_URL.'/api/customers', ['json' => [
            "name" => "test",
            "surname" => "test",
            "socialReason" => "test",
            "email" => "test@test.fr",
            "phoneNumber" => "0555028881",
            "streetNumber" => "12",
            "streetName" => "test",
            "additionalAddress" => "test",
            "city" => $city
        ]]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertRegExp('~^/api/customers/\d+$~', $response->toArray()['@id']);
        $this->assertMatchesResourceItemJsonSchema(Customer::class);
    }

    public function testPutNoAuth(): void
    {
        static::createClient()->request('PUT', static::findFirstIri(Customer::class), ['json' => [
            'name' => 'test',
        ]]);

        $this->assertResponseStatusCodeSame(401, "JWT Token not found");
        $this->assertResponseHeaderSame('content-type', 'application/json');
    }

    /*
     *  ROLE_USER TESTING
     */

    public function testGetCollectionRoleUser(): void
    {
        $jwt = static::getJWTTokenWithRole('user');

        $client = static::createClient();

        $response = $client->request('GET', self::BASE_URL.'/api/customers', ['auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertMatchesResourceCollectionJsonSchema(Customer::class);
        $this->assertCount(2, $response->toArray()['hydra:member']);
    }

    public function testGetItemRoleUser(): void
    {
        $jwt = static::getJWTTokenWithRole('user');

        $client = static::createClient();

        $iri = static::findFirstIri(Customer::class);

        $client->request('GET', $iri, ['auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(403);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
    }

    public function testPostRoleUser(): void
    {
        $jwt = static::getJWTTokenWithRole('user');

        $client = static::createClient();
        $city = static::findFirstIri(City::class);
        $response = $client->request('POST', self::BASE_URL.'/api/customers', ['json' => [
            "name" => "test",
            "surname" => "test",
            "socialReason" => "test",
            "email" => "test@test.fr",
            "phoneNumber" => "0555028881",
            "streetNumber" => "12",
            "streetName" => "test",
            "additionalAddress" => "test",
            "city" => $city
        ], 'auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertRegExp('~^/api/customers/\d+$~', $response->toArray()['@id']);
        $this->assertMatchesResourceItemJsonSchema(Customer::class);
        $this->assertMatchesJsonSchema('
            {
              "@context": "/api/contexts/Customer",
              "@id": "/api/customers/1892",
              "@type": "Customer",
              "id": 1892,
              "name": "string",
              "surname": "string",
              "email": "string@tessss.fr",
              "phoneNumber": "0555028881",
              "socialReason": "string",
              "customerRequests": [],
              "streetNumber": "string",
              "streetName": "string",
              "additionalAddress": "string",
              "city": "/api/cities/1"
            }
        ');
    }

    public function testPutRoleUser(): void
    {
        $jwt = static::getJWTTokenWithRole('user');
        $client = static::createClient();

        $client->request('PUT', static::findFirstIri(Customer::class), ['json' => [
            'name' => 'test',
        ], 'auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(403);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
    }

    /*
     *  ROLE_ADMIN TESTING
     */

    public function testGetCollectionRoleAdmin(): void
    {
        $jwt = static::getJWTTokenWithRole('admin');

        $client = static::createClient();

        $response = $client->request('GET', self::BASE_URL.'/api/customers', ['auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertMatchesResourceCollectionJsonSchema(Customer::class);
        $this->assertCount(3, $response->toArray()['hydra:member']);
    }

    public function testGetItemRoleAdmin(): void
    {
        $jwt = static::getJWTTokenWithRole('admin');

        $client = static::createClient();

        $iri = static::findFirstIri(Customer::class);

        $client->request('GET', $iri, ['auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertMatchesJsonSchema('
            {
              "@context": "/api/contexts/Customer",
              "@id": "/api/customers/1715",
              "@type": "Customer",
              "id": 1715,
              "name": "Guyot",
              "surname": "Daniel",
              "email": "william77@dbmail.com",
              "phoneNumber": "0555028881",
              "socialReason": "Lemonnier S.A.",
              "customerRequests": [
              ],
              "streetNumber": "12",
              "streetName": "rue de Gautier",
              "additionalAddress": "Chambre 570",
              "city": "/api/cities/1595"
            }
        ');
    }

    public function testPostRoleAdmin(): void
    {
        $jwt = static::getJWTTokenWithRole('admin');

        $client = static::createClient();
        $city = static::findFirstIri(City::class);
        $response = $client->request('POST', self::BASE_URL.'/api/customers', ['json' => [
            "name" => "test",
            "surname" => "test",
            "socialReason" => "test",
            "email" => "test@test.fr",
            "phoneNumber" => "0555028881",
            "streetNumber" => "12",
            "streetName" => "test",
            "additionalAddress" => "test",
            "city" => $city
        ], 'auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertRegExp('~^/api/customers/\d+$~', $response->toArray()['@id']);
        $this->assertMatchesResourceItemJsonSchema(Customer::class);
        $this->assertMatchesJsonSchema('
            {
              "@context": "/api/contexts/Customer",
              "@id": "/api/customers/1892",
              "@type": "Customer",
              "id": 1892,
              "name": "string",
              "surname": "string",
              "email": "string@tessss.fr",
              "phoneNumber": "0555028881",
              "socialReason": "string",
              "customerRequests": [],
              "streetNumber": "string",
              "streetName": "string",
              "additionalAddress": "string",
              "city": "/api/cities/1"
            }
        ');
    }

    public function testPutRoleAdmin(): void
    {
        $jwt = static::getJWTTokenWithRole('admin');
        $client = static::createClient();

        $client->request('PUT', static::findFirstIri(Customer::class), ['json' => [
            'name' => 'test',
        ], 'auth_bearer' => $jwt]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
    }
}